# Aliyun ECS Worker
> This is a CLI for aliyun ecs operation, can be started as a server to connect with others by rabbitmq server.
> For more details of ECS, please refer [Aliyun ECS Help Documention][1].
## Platform
> python2
## Requirement package
|Package|Version|
|:-----:|:-----:|
|aliyun-python-sdk-core|2.3.2|
|aliyun-python-sdk-ecs|3.1.0|
[1]: https://help.aliyun.com/product/25365.html

