#!/usr/bin/env python
# coding=utf-8
import argparse
import signal
import sys
import time
import json

from common.log import init, info, warn, debug, error
from common.aliyun_product_operation_interface import ALiYunRdsOperation

ALIYUN_RDS_OPERATION_LOG = "aliyun_rds.log"

def test_case_1():
    # create rds
    debug("===== create rds")
    res = aliy_op.CreateDBInstance(Engine="MySQL", EngineVersion="5.7", SecurityIPList="0.0.0.0/0",
                                 PayType="Postpaid", DBInstanceClass="mysql.n1.micro.1", InstanceNetworkType="VPC",
                                 VpcId="vpc-bp12kn5y3gvxe4u21yyis", VSwitchId="vsw-bp1rd12gag1q4ejo2ft5p",
                                 DBInstanceStorage=20, DBInstanceNetType="Intranet", ZoneId="cn-hangzhou-b",
                                 DBInstanceDescription="test rds creation.", ConnectionMode="Performance")
    debug("===== result is %s" % (res))

def test_case_2():
    debug("===== delete rds")
    res = aliy_op.DeleteDBInstance(DBInstanceId="rm-bp1swsxmftc0ok55k")
    debug("===== result is %s" % (res))

def test_case_3():
    debug("===== delete rds pub conn")
    res = aliy_op.ReleaseInstancePublicConnection(DBInstanceId="rm-bp1cfj1674900xo36",
                                                  CurrentConnectionString="rdsautopublicconn333")
    debug("===== result is %s" % (res))

def test_case_4():
    debug("===== describe rds attribute")
    res = aliy_op.DescribeDBInstanceAttribute(DBInstanceId="rm-bp1i912y5bnsp2ksr")
    debug("===== result is %s" % (res))

def test_case_5():
    debug("===== describe rds connection string")
    res = aliy_op.DescribeDBInstanceNetInfo(DBInstanceId="rm-bp1tuoly1hni0wcf5")
    debug("===== result is %s" % (res))

if __name__ == "__main__":
    init(logpath=ALIYUN_RDS_OPERATION_LOG)
    # ak = "LTAIr599QLK8bBhU"
    # sk = "HD8PRGYt1WIOJhqIbLF89bQoO8g4uK"
    # region_id = "cn-hangzhou"
    ak = "LTAIliklfIP300rx"
    sk = "uJRyp2uUmG0TWaXbuS0oKhb5BALgFo"
    region_id = "cn-beijing"
    global aliy_op
    aliy_op = ALiYunRdsOperation(ak, sk, region_id)
    # test_case_1()
    test_case_2()
    # test_case_3()
    # test_case_4()
    # test_case_5()
