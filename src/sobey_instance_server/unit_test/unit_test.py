#!/usr/bin/env python
# coding=utf-8
import argparse
import signal
import sys
import time
import json

from common.log import init, info, warn, debug, error
from common.vmware_vcenter_interface import VmwareVcenterOperationInterface

SOBEY_ECS_OPERATION_LOG = "sobey_instance.log"

def test_case_1():
    debug("=====create instance")
    res = voi.CreateInstance(RegionId="CD_IDC", ImageId="test-safe", ZoneId="Cluster1",
                             InstanceName="test-fzb-create", InstanceType="test.n1",
                             VmFolderName="vpc-test-fzb", DatastoreId="vsanDatastore_1")
    debug("result is %s." % (res))

if __name__ == "__main__":
    init(logpath=SOBEY_ECS_OPERATION_LOG)
    host = "10.10.4.109"
    port = 443
    user = "administrator@vsphere.local"
    password = "Newmed!@s0bey"
    global voi
    voi = VmwareVcenterOperationInterface(host, port, user, password)
    test_case_1()
