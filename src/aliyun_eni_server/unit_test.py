#!/usr/bin/env python
# coding=utf-8
import argparse
import signal
import sys
import time
import json

from common.log import init, info, warn, debug, error
from common.aliyun_product_operation_interface import ALiYunEniOperation

ALIYUN_ENI_OPERATION_LOG = "aliyun_eni.log"

def test_case_1():
    # create eni
    debug("===== Create ENI")
    res = aliy_op.CreateNetworkInterface(VSwitchId="vsw-uf6ozjfienyubgxf4ab8k",
                                         SecurityGroupId="sg-uf63fivqyqk4dhfjbivb",
                                         NetworkInterfaceName="test-eni-create-01",
                                         Description="test eni create 01")
    debug(res)
    debug("===== End of ENI Creation")

def test_case_2():
    # attach eni
    debug("===== Attach ENI")
    res = aliy_op.AttachNetworkInterface(NetworkInterfaceId="eni-uf6fe3jh67t7fgyfdwva",
                                         InstanceId="i-uf6ha749626g57akada4")
    debug(res)
    debug("===== End of ENI Attach")

def test_case_3():
    # describe eni
    debug("===== Describe ENIs")
    res = aliy_op.DescribeNetworkInterfaces(VSwitchId="vsw-uf6ozjfienyubgxf4ab8k",
                                            SecurityGroupId="sg-uf63fivqyqk4dhfjbivb")
    debug(res)
    debug("===== End of ENI Describe")

def test_case_4():
    # detach eni
    debug("===== Detach ENI")
    res = aliy_op.DetachNetworkInterface(NetworkInterfaceId="eni-uf6fks928wwlzoo7been",
                                         InstanceId="i-uf6aelo21k0cpifsmodm")
    debug(res)
    debug("===== End of ENI Detach")

def test_case_5():
    # modify eni
    debug("===== Modify ENI")
    res = aliy_op.ModifyNetworkInterfaceAttribute(NetworkInterfaceId="eni-uf6fks928wwlzoo7been",
                                                  NetworkInterfaceName="test-eni-modify-01",
                                                  Description="test eni modify 01",
                                                  SecurityGroupIds=["sg-uf63fivqyqk4dhfjbivb", "sg-uf68b5kjkfvu00zafygs"])
    debug(res)
    debug("===== End of ENI Attribute Modify")

def test_case_6():
    # delete eni
    debug("===== Delete ENI")
    res = aliy_op.DeleteNetworkInterface(NetworkInterfaceId="eni-uf6atq3wv11v5xm02lw5")
    debug(res)
    debug("===== End of ENI Delete")

if __name__ == "__main__":
    init(logpath=ALIYUN_ENI_OPERATION_LOG)
    # ak = "LTAIr599QLK8bBhU"
    # sk = "HD8PRGYt1WIOJhqIbLF89bQoO8g4uK"
    region_id = "cn-shanghai"
    ak = "LTAIliklfIP300rx"
    sk = "uJRyp2uUmG0TWaXbuS0oKhb5BALgFo"
    # region_id = "cn-beijing"
    global aliy_op
    aliy_op = ALiYunEniOperation(ak, sk, region_id)
    
    # test_case_1()
    test_case_2()
    # test_case_3()
    # test_case_4()
    # test_case_5()
    # test_case_6()
