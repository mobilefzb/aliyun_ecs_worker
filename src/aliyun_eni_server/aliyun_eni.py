#!/usr/bin/env python
# coding=utf-8
import argparse
import signal
import sys
import time
import json
import traceback

from common.log import init, info, warn, debug, error
from common.rabbitmq_interface import RabbitmqService, pika
from common.utils import byteify
from common.aliyun_product_operation_interface import ALiYunEniOperation

from common.setting import Cmop2ParametersAdapter, ConfigureDiscovery

ALIYUN_SERVER_LOG = "aliyun_eni.log"
ALIYUN_SERVER_CONF = "service.conf"
COMPUTE_EXCHANGE = "vulcanus-iaas-network"
LOG_EXCHANGE = "vulcanus-iaas-log"
TASK_ROUTE_KEY = "vulcanus.task.create"

def _signal_handler(signum, frame):

    info("System exit.")
    sys.exit(0)

def task_report(req, repo, route_key):

    task_rep = {}
    rs = RabbitmqService()
    task_rep["taskCode"] = repo["taskCode"]
    task_rep["action"] = route_key
    task_rep["username"] = repo["username"]
    task_rep["status"] = repo["statusCode"]
    task_rep["requestData"] = req
    task_rep["responseData"] = json.dumps(repo)
    if isinstance(repo["infos"]["requestId"], list):
        for request_id in repo["infos"]["requestId"]:
            task_rep["requestId"] = request_id
            tr_body = json.dumps(task_rep)
            rs.publish(exchange=LOG_EXCHANGE, routing_key=TASK_ROUTE_KEY, message=tr_body)
    else:
        task_rep["requestId"] = repo["infos"]["requestId"]
        tr_body = json.dumps(task_rep)
        rs.publish(exchange=LOG_EXCHANGE, routing_key=TASK_ROUTE_KEY, message=tr_body)

def rpc_query_common(func):

    def wrapper(method, properties, body):
        info("[x] Received %r" % (body))
        info("[x] Begin to do %s ..." % (method.routing_key))
        # publish message to vulcanus-iaas-log access successful
        rs = RabbitmqService()
        req = json.loads(body, object_hook=byteify)

        # convert the data to current format.
        cmop2_adapter = Cmop2ParametersAdapter()
        request_info = cmop2_adapter.input_adapt(req)

        repo = func(**request_info)
        repo["StatusCode"] = repo["Infos"]["StatusCode"]
        repo["Message"] = repo["Infos"]["Message"]
        repo["RequestId"] = repo["Infos"]["RequestId"]
        response_info = cmop2_adapter.output_adapt(repo)
        rs.publish(exchange='', routing_key=properties.reply_to,
                   properties=pika.BasicProperties(correlation_id=properties.correlation_id),
                   message=response_info["infos"]["result"])

        # publish message to vulcanus-iaas-log with task report
        task_report(body, response_info, "%s.result" % (method.routing_key))

    return wrapper

@rpc_query_common
def rpc_query_networkinterfaces(ak, sk, region_id, **kwargs):
    aliy_op = ALiYunEniOperation(ak, sk, region_id)
    repo = {}
    ret = aliy_op.DescribeNetworkInterfaces(**kwargs)
    repo["Infos"] = ret
    repo = dict(kwargs, **repo)

    return repo

def rpc_process(ch, method, properties, body):

    # debug("Get rpc call body : %r" % (body))
    ALIYUN_RPC_CALLBACK_DICT = {
        "vulcanus.eni.query.list": rpc_query_networkinterfaces
    }
    # report to log the rpc is began
    ch.basic_publish(exchange=LOG_EXCHANGE, routing_key="result.access.successful",
                     body="Accept rpc request successful!")

    if method.routing_key in ALIYUN_RPC_CALLBACK_DICT:
        evfunc = ALIYUN_RPC_CALLBACK_DICT[method.routing_key]
        evfunc(method, properties, body)
        # publish message to vulcanus-iaas-log execute complete
        ch.basic_publish(exchange=LOG_EXCHANGE, routing_key="result.access.successful",
                         body="Process %s request complete!" % (method.routing_key))
    else:
        msg = "There is no callback to process request %s." % (method.routing_key)
        ch.basic_publish(exchange=LOG_EXCHANGE, routing_key="result.access.failure",
                         body=msg)

    ch.basic_ack(delivery_tag=method.delivery_tag)

def event_process_common(func):
    def wrapper(method, properties, body):
        info("[x] Received %r" % (body))
        info("[x] Begin to do %s ..." % (method.routing_key))
        # publish message to vulcanus-iaas-log access successful
        rs = RabbitmqService()

        # get basic info
        req = json.loads(body, object_hook=byteify)

        # convert the data to current format.
        cmop2_adapter = Cmop2ParametersAdapter()
        request_info = cmop2_adapter.input_adapt(req)
        repo = {}
        try:
            repo = func(**request_info)
        except Exception as ex:
            repo["StatusCode"] = "Error"
            repo["Message"] = "Failed to execute %s with some exceptions %s." % \
                              (func.__name__, traceback.format_exc())
            repo["Infos"] = {}
            repo["RequestId"] = ""
            repo = dict(request_info, **repo)

        response_info = cmop2_adapter.output_adapt(repo)

        msg = json.dumps(response_info)

        # publish message to vulcanus-iaas-computer with execute result
        result_key = "%s.result" % (method.routing_key)
        rs.publish(exchange=COMPUTE_EXCHANGE, routing_key=result_key,
                   message=msg)

        # publish message to vulcanus-iaas-log with task report
        task_report(body, response_info, result_key)

    return wrapper

@event_process_common
def event_create_eni(ak, sk, region_id, **kwargs):
    repo = {}
    aliy_op = ALiYunEniOperation(ak, sk, region_id)
    ret = aliy_op.CreateNetworkInterface(**kwargs)
    repo = dict(kwargs, **ret)
    rs = RabbitmqService()
    # wait the eni status become Available
    if repo["StatusCode"] == "200":
        eni_ids = [repo["NetworkInterfaceId"]]
        # check the status of eni when it's available.
        for i in range(30):
            qs_res = aliy_op.DescribeNetworkInterfaces(NetworkInterfaceIds=eni_ids)
            if not qs_res["NetworkInterfaceSets"]["NetworkInterfaceSet"]:
                warn("Check ENI status error: can not get any ENI %s" % (json.dumps(qs_res)))
            elif qs_res["NetworkInterfaceSets"]["NetworkInterfaceSet"][0]["Status"] == "Available":
                # get the primary ip of the eni
                if qs_res["NetworkInterfaceSets"]["NetworkInterfaceSet"]:
                    private_ip = qs_res["NetworkInterfaceSets"]["NetworkInterfaceSet"][0]["PrivateIpAddress"]
                    repo["PrimaryIpAddress"] = private_ip
                break
            rs.sleep(5)
        else:
            warn("Check ENI %s status timeout." % (repo["NetworkInterfaceId"]))

    return repo

@event_process_common
def event_delete_eni(ak, sk, region_id, **kwargs):
    repo = {}
    aliy_op = ALiYunEniOperation(ak, sk, region_id)

    ret = aliy_op.DeleteNetworkInterface(**kwargs)
    repo = dict(kwargs, **ret)

    return repo

def _event_modify_common(op_func, update_keys, db_func=None, **kwargs):
    repo = {}
    kwargs_keys = set(kwargs.keys())
    k_update_keys = update_keys & kwargs_keys
    if k_update_keys:
        update_dict = {}
        for update_key in k_update_keys:
            update_dict[update_key] = kwargs[update_key]
        ret = op_func(**kwargs)
        if "Filters" not in kwargs:
            kwargs["Filters"] = {"NetworkInterfaceId": kwargs["NetworkInterfaceId"]}

        if db_func:
            db_func(update_dict=update_dict, **kwargs["Filters"])

        repo = dict(kwargs, **ret)

        if "Infos" not in repo:
            repo["Infos"] = {}

        for k, v in update_dict.items():
            nk = "%s%s" % (k[0].lower(), k[1:])
            repo["Infos"][nk] = v
    else:
        repo["StatusCode"] = "Error"
        repo["Message"] = "Failed to get any parameters to update (%s)." % (op_func.__name__)
        repo["Infos"] = {}
        repo["RequestId"] = ""
        repo = dict(repo, **kwargs)

    return repo

@event_process_common
def event_modify_networkinterfaceattribute(ak, sk, region_id, **kwargs):
    aliy_op = ALiYunEniOperation(ak, sk, region_id)
    update_keys = {"NetworkInterfaceId", "NetworkInterfaceName", "Description", "SecurityGroupIds"}
    # 2018.1.9 now we only support one security group in our rest api
    kwargs["SecurityGroupIds"] = [kwargs["SecurityGroupId"]]
    repo = _event_modify_common(aliy_op.ModifyNetworkInterfaceAttribute, update_keys,
                                None, **kwargs)

    return repo

def _event_operation_common(op_func, **kwargs):
    repo = {}
    ret = op_func(**kwargs)
    repo = dict(kwargs, **ret)

    return repo

@event_process_common
def event_eni_attach_networkinterface(ak, sk, region_id, **kwargs):
    aliy_op = ALiYunEniOperation(ak, sk, region_id)
    repo = _event_operation_common(aliy_op.AttachNetworkInterface, **kwargs)
    return repo

@event_process_common
def event_eni_detach_networkinterface(ak, sk, region_id, **kwargs):
    aliy_op = ALiYunEniOperation(ak, sk, region_id)
    repo = _event_operation_common(aliy_op.DetachNetworkInterface, **kwargs)
    return repo

def eni_event_process(ch, method, properties, body):

    ALIYUN_EVENT_CALLBACK_DICT = {
        "vulcanus.eni.create": event_create_eni,
        "vulcanus.eni.delete": event_delete_eni,
        "vulcanus.eni.modify": event_modify_networkinterfaceattribute,
        "vulcanus.eni.operation.attach": event_eni_attach_networkinterface,
        "vulcanus.eni.operation.detach": event_eni_detach_networkinterface
    }

    ch.basic_publish(exchange=LOG_EXCHANGE, routing_key="result.access.successful",
                     body="Accept %s request successful!" % (method.routing_key))

    if method.routing_key in ALIYUN_EVENT_CALLBACK_DICT:
        evfunc = ALIYUN_EVENT_CALLBACK_DICT[method.routing_key]
        evfunc(method, properties, body)
        # publish message to vulcanus-iaas-log execute complete
        ch.basic_publish(exchange=LOG_EXCHANGE, routing_key="result.access.successful",
                         body="Process %s request complete!" % (method.routing_key))
    else:
        msg = "There is no callback to process request %s." % (method.routing_key)
        ch.basic_publish(exchange=LOG_EXCHANGE, routing_key="result.access.failure",
                         body=msg)

    ch.basic_ack(delivery_tag=method.delivery_tag)

def start_rabbitmq_mode(args):
    # consumer event create.instance
    # publish event create.instance.log
    # publish event create.instance.stats
    host = args.rabbitmq_host
    port = args.rabbitmq_port
    user = args.rabbitmq_user
    password = args.rabbitmq_password
    rs = RabbitmqService(host, port, user, password)

    # create a channel, if one message can't be ack
    # the queue will be ignore
    rs.channel_create(prefetch_count=1)

    # # create a rpc server
    # rs.create_rpc_server(queue_name="Vulcanus.ecs.query", rpc_callback=rpc_process)

    # create two exchanges
    # declare exchange
    rs.exchange_declare(exchange=COMPUTE_EXCHANGE, type="topic", durable=True)
    rs.exchange_declare(exchange=LOG_EXCHANGE, type="fanout", durable=True)

    # bind queue
    # args = {"x-message-ttl": 60000}
    # related route keys
    route_keys = ["vulcanus.eni.create", "vulcanus.eni.delete", "vulcanus.eni.modify",
                  "vulcanus.eni.operation.attach", "vulcanus.eni.operation.detach"]
    q_name = "aliyun_eni_service"
    rs.queue_bind(COMPUTE_EXCHANGE, route_keys=route_keys,
                  queue_name=q_name, durable=True)
    rs.regist_consume(q_name, eni_event_process)

    # bind queue for rpc
    route_keys = ["vulcanus.eni.query.list"]
    q_name = "aliyun_eni_rpc"
    rs.queue_bind(COMPUTE_EXCHANGE, route_keys=route_keys,
                  queue_name=q_name, durable=True)
    rs.regist_consume(q_name, rpc_process)

    # start consuming
    rs.start_consuming()
    signal.signal(signal.SIGINT, _signal_handler)
    rs.consuming_loop()

def argument_parser():
    parser = argparse.ArgumentParser(description="Aliyun ENI Operation CLI.")

    # create actions subcommand
    sparsers = parser.add_subparsers(title="Actions", description="Actions of the service.",
                                     help="Action name, use -h after it for more details.")
    # start rabbitmq server
    sparser_rabbitmq_conn = sparsers.add_parser("start-rabbitmq-to-connect",
                                                help="Start as a worker service for API Server events exchange. Note: if you have multiple events to confim and public plse use config file.")
    sparser_rabbitmq_conn.set_defaults(func=start_rabbitmq_mode)

    sparser_rabbitmq_conn.add_argument("-H", "--host", action="store", type=str, dest="rabbitmq_host",
                                       help="Rabbitmq server host to connect.", required=False, default="localhost")
    sparser_rabbitmq_conn.add_argument("-p", "--port", action="store", type=int, dest="rabbitmq_port",
                                       help="Rabbitmq server port to connect.", required=False, default=5672)
    sparser_rabbitmq_conn.add_argument("-u", "--user", action="store", type=str, dest="rabbitmq_user",
                                       help="Rabbitmq server user to connect.", required=False, default="guest")
    sparser_rabbitmq_conn.add_argument("-P", "--password", action="store", type=str, dest="rabbitmq_password",
                                       help="Rabbitmq server password to connect.", required=False, default="guest")
    sparser_rabbitmq_conn.add_argument("-c", "--config", action="store", type=str, dest="conf_path",
                                       help="Server config file.", required=False, default=ALIYUN_SERVER_CONF)
    sparser_rabbitmq_conn.add_argument("-d", "--daemon", action="store", type=bool, dest="daemon",
                                       help="Server daemonized flag.", required=False, default=False)
    sparser_rabbitmq_conn.add_argument("-l", "--log-path", action="store", type=str, dest="log_path",
                                       help="Server log path.", required=False, default=ALIYUN_SERVER_LOG)

    return parser.parse_args()

if __name__ == "__main__":
    args = argument_parser()
    init(logpath=args.log_path)
    cd = ConfigureDiscovery()
    cmop2_adapter = Cmop2ParametersAdapter()
    cd.regist_observer("input_mapping", cmop2_adapter.input_notify)
    cd.regist_observer("output_mapping", cmop2_adapter.output_notify)
    cd.start_observe_conf(conf_file=args.conf_path)
    cd.notify()
    args.func(args)
