#!/usr/bin/env python
# coding=utf-8
import argparse
import signal
import sys
import time
import json

from common.log import init, info, warn, debug, error
from common.aliyun_product_operation_interface import ALiYunInstanceOperation
from aliyun_key_pair_db_model import AliyunSSHKeyPairDBExecutor, AliyunSSHKeyPairBindDBExecutor

ALIYUN_ECS_OPERATION_LOG = "aliyun_ssh_keypair.log"

def test_case_1():
    debug("=====Test Aliyun SSH Key Pair Table.")
    DB_USER = "aliyun_ecs"
    DB_PASS = "sobey@2016"
    DB_HOST = "172.17.0.2"
    DB_PORT = "3306"
    DB_NAME = "aliyun_ecs_db"
    DATABASE_URI = 'mysql+mysqldb://%s:%s@%s:%s/%s' % (DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
    aliy_db = AliyunSSHKeyPairDBExecutor(db_url=DATABASE_URI)
    aliy_db_2 = AliyunSSHKeyPairBindDBExecutor(db_url=DATABASE_URI)
    aliy_db.insert(TaskCode="abcdefg", KeyPairName="testtest", RegionId="cn-hangzhou",
                   KeyPairFingerprint="ABCDEFG1234", PrivateKeyBody="privatekeybody",
                   PublicKeyBody="publickeybody")
    # query all
    debug("=====query all")
    query_res = aliy_db.query()
    debug(query_res)
    # delete all
    debug("=====delete all")
    delete_res = aliy_db.delete()

def test_case_2():
    debug("=====Test Aliyun SSH Key Pair Bind Table.")
    DB_USER = "aliyun_ecs"
    DB_PASS = "sobey@2016"
    DB_HOST = "172.17.0.2"
    DB_PORT = "3306"
    DB_NAME = "aliyun_ecs_db"
    DATABASE_URI = 'mysql+mysqldb://%s:%s@%s:%s/%s' % (DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
    aliy_db = AliyunSSHKeyPairBindDBExecutor(db_url=DATABASE_URI)
    aliy_db.insert(TaskCode="abcdefg", InstanceId="i-123fdafd2f", KeyPairName="dfsdfdfwerwe")
    # query all
    debug("=====query all")
    query_res = aliy_db.query()
    debug(query_res)
    # delete all
    debug("=====delete all")
    delete_res = aliy_db.delete()

def test_case_3():
    debug("=====Test Aliyun SSH Key Pair tables operation")
    DB_USER = "aliyun_ecs"
    DB_PASS = "sobey@2016"
    DB_HOST = "172.17.0.2"
    DB_PORT = "3306"
    DB_NAME = "aliyun_ecs_db"
    DATABASE_URI = 'mysql+mysqldb://%s:%s@%s:%s/%s' % (DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
    aliy_db_kp = AliyunSSHKeyPairDBExecutor(db_url=DATABASE_URI)
    aliy_db_kpb = AliyunSSHKeyPairBindDBExecutor(db_url=DATABASE_URI)

    debug("===== insert a new ssh key pair.")
    aliy_db_kp.insert(TaskCode="task-code-12345", KeyPairName="testtest", RegionId="cn-hangzhou",
                   KeyPairFingerprint="ABCDEFG1234", PrivateKeyBody="privatekeybody",
                   PublicKeyBody="publickeybody")
    debug("===== bind it with a new instance")
    kp_info = aliy_db_kp.query(TaskCode="task-code-12345")
    debug(kp_info)
    aliy_db_kpb.insert(TaskCode=kp_info[0]["TaskCode"], InstanceId="i-123fdafd2f",
                       KeyPairName=kp_info[0]["KeyPairName"])
    kpb_info = aliy_db_kpb.query(TaskCode="task-code-12345")
    debug(kpb_info)
    debug("===== update key pair to a new instance.")
    aliy_db_kpb.update(update_dict={"InstanceId":"i-456fdafd2f"}, TaskCode=kp_info[0]["TaskCode"])
    kpb_info = aliy_db_kpb.query(TaskCode="task-code-12345")
    debug(kpb_info)
    debug("=====delete all")
    aliy_db_kp.delete()
    aliy_db_kpb.delete()

def test_case_4():
    debug("===== delete with multiple key")
    aliy_db_skpb = AliyunSSHKeyPairBindDBExecutor()
    aliy_db_skpb.delete(KeyPairName="test_keypair_name",
                        InstanceId="i-bp1i0q74l7rth7sta1y7")

if __name__ == "__main__":
    init(logpath=ALIYUN_ECS_OPERATION_LOG)
    ak = "LTAIr599QLK8bBhU"
    sk = "HD8PRGYt1WIOJhqIbLF89bQoO8g4uK"
    region_id = "cn-hangzhou"
    global aliy_op

    # test_case_1()
    # test_case_2()
    # test_case_3()
    test_case_4()
