# coding=utf-8

from common.database import *

class AliyunSSHKeyPair(BaseModel):
    __tablename__ = 'sshkeypair_tbl'
    TaskCode = Column(String(200), primary_key=True) # vulcanus 的 request_id
    KeyPairName = Column(String(256)) # 密钥对的名字，创建或者导入后生成，必须
    RegionId = Column(String(64)) # region id, 必须
    KeyPairFingerprint = Column(String(48)) # 密钥对的指纹，绑定密钥对名字，必须
    PrivateKeyBody = Column(Text(65535)) # 密钥对的私钥，必须
    PublicKeyBody = Column(Text(65535)) # 密钥对的公钥，必须

class AliyunSSHKeyPairBind(BaseModel):
    __tablename__ = 'sshkeypair_bind_tbl'
    TaskCode = Column(String(200), primary_key=True) # vulcanus 的 request_id
    InstanceId = Column(String(256)) # 密钥对绑定的instance的id，非必须
    KeyPairName = Column(String(256)) # 密钥对的名字，创建或者导入后生成，必须

class AliyunSSHKeyPairDBExecutor(CommonDBExecutor):

    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(AliyunSSHKeyPairDBExecutor, self).__init__(db_url=db_url, table=AliyunSSHKeyPair)

class AliyunSSHKeyPairBindDBExecutor(CommonDBExecutor):

    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(AliyunSSHKeyPairBindDBExecutor, self).__init__(db_url=db_url, table=AliyunSSHKeyPairBind)
