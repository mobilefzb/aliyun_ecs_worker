#!/usr/bin/env python
# coding=utf-8
import argparse
import signal
import sys
import time
import json

from common.log import init, info, warn, debug, error
from common.aliyun_product_operation_interface import ALiYunImageOperation

import aliyun_image_db_model
from aliyun_image_db_model import AliyunImageDBExecutor, AliyunImageDevMapDBExecutor, \
    AliyunImageInstanceDBExecutor, AliyunImageSnapshotDBExecutor, AliyunImageSharePermission
from common.setting import Cmop2ParametersAdapter, ConfigureDiscovery

ALIYUN_ECS_OPERATION_LOG = "aliyun_image.log"

def test_case_1():
    debug("===== test image create from snapshot id")
    ret = aliy_op.CreateImage(SnapshotId="s-bp164qwjbi8milc95nvq", ImageName="test-image-fzb-01",
                              ImageVersion="0.0.1", Description="test image create.")
    debug(ret)

def test_case_2():
    debug("===== test image create from instance id")
    ret = aliy_op.CreateImage(InstanceId="i-bp1g6vdok72lr9edxq3m", ImageName="test-image-fzb-02",
                              ImageVersion="0.0.2", Description="test image create.")
    debug(ret)

def test_case_3():
    debug("===== test image create from device mapping")

def test_case_4():
    debug("===== test image modify.")
    ret = aliy_op.ModifyImageAttribute(ImageId="m-bp16z4q15uuddug6k37d", ImageName="test-image-fzb-001",
                                       Description="test image modify.")
    debug(ret)

def test_case_5():
    debug("===== test image copy.")
    ret = aliy_op.CopyImage(ImageId="m-bp16z4q15uuddug6k37d", DestinationRegionId="cn-shanghai",
                            DestinationImageName="test-image-fzb-003",
                            DestinationDescription="test image copy")
    debug(ret)

def test_case_6():
    debug("===== test image copy cancel.")
    debug("===== begin to copy.")
    ret = aliy_op.CopyImage(ImageId="m-bp16z4q15uuddug6k37d", DestinationRegionId="cn-shanghai",
                            DestinationImageName="test-image-fzb-004",
                            DestinationDescription="test image copy and cancel.")
    debug(ret)
    new_img_id = ret["ImageId"]
    time.sleep(10)
    debug("===== begin to cancel.")
    ret = aliy_op.CancelCopyImage(ImageId=new_img_id)
    debug(ret)

def test_case_7():
    debug("===== test image copy cancel.")
    ret = aliy_op.CancelCopyImage(ImageId="m-uf6glf2r6p2zapykkvls")
    debug(ret)

def test_case_8():
    debug("===== test image share permission add account.")
    ret = aliy_op.ModifyImageSharePermission(ImageId="m-bp18zujb2gwl5sed8nqh",
                                             AddAccount1="1331197081468210")
    debug(ret)

def test_case_9():
    debug("===== test image share permission remove account.")
    ret = aliy_op.ModifyImageSharePermission(ImageId="m-bp18zujb2gwl5sed8nqh",
                                             RemoveAccount1="1331197081468210")
    debug(ret)

def test_case_10():
    debug("===== test image import.")
    ret = aliy_op.ImportImage(ImageName="test-ce7-fzb-01", Description="test image import.",
                              Architecture="x86_64", DiskDeviceMapping1Format="RAW",
                              DiskDeviceMapping1OSSBucket="fzbimagetest",
                              DiskDeviceMapping1OSSObject="centostest_m-bp18zujb2gwl5sed8nqh_t-bp1cusv1by2rwbyoz5ys.raw",
                              DiskDeviceMapping1DiskImageSize="55")
    debug(ret)

def test_case_11():
    debug("===== test image delete.")
    ret = aliy_op.DeleteImage(ImageId="m-bp10zgaj3hnebrtghy4c", Force=True)
    debug(ret)

def test_case_12():
    debug("===== test image describe.")
    ret = aliy_op.DescribeImages(ImageId="m-bp10zgaj3hnebrtghy4c")
    debug(ret)

def test_case_13():
    debug("===== test image share permission.")
    ret = aliy_op.DescribeImageSharePermission(ImageId="m-bp18zujb2gwl5sed8nqh")
    debug(ret)

def test_case_14():
    debug("===== test image describe.")
    ret = aliy_op.DescribeImages(ImageOwnerAlias="self")
    debug(ret)

def test_case_15():
    debug("===== test image database.")
    cd = ConfigureDiscovery()
    cd.regist_observer("db", aliyun_image_db_model.notify)
    cd.start_observe_conf(conf_file="service.conf")
    cd.notify()
    aliy_db = AliyunImageDBExecutor()
    rec = aliy_db.query(ImageId="m-bp16p7abljvv71n6h8sf")
    debug(rec)

def test_case_16():
    debug("===== test image database insert and delete.")
    cd = ConfigureDiscovery()
    cd.regist_observer("db", aliyun_image_db_model.notify)
    cd.start_observe_conf(conf_file="service.conf")
    cd.notify()
    aliy_db = AliyunImageDBExecutor()
    aliy_db.delete(ImageId="m-uf6gxz3rn0itxfs76k3j")

if __name__ == "__main__":
    init(logpath=ALIYUN_ECS_OPERATION_LOG)
    # egs_vdi_test
    ak = "LTAIr599QLK8bBhU"
    sk = "HD8PRGYt1WIOJhqIbLF89bQoO8g4uK"
    # zhangchi_hqy
    # ak = "LTAIliklfIP300rx"
    # sk = "uJRyp2uUmG0TWaXbuS0oKhb5BALgFo"
    region_id = "cn-hangzhou"
    # region_id = "cn-shanghai"
    global aliy_op
    aliy_op = ALiYunImageOperation(ak, sk, region_id)
    # test_case_1()
    # test_case_2()
    # test_case_4()
    # test_case_5()
    # test_case_6()
    # test_case_7()
    # test_case_8()
    # test_case_9()
    # test_case_10()
    test_case_11()
    # test_case_12()
    # test_case_13()
    # test_case_14()
    # test_case_15()
    # test_case_16()
