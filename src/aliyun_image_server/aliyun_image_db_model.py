# coding=utf-8

from common.database import *

class AliyunImageSnapshot(BaseModel):
    __tablename__ = 'image_sys_snapshot_tbl'
    TaskCode = Column(String(200), primary_key=True) # vulcanus 的 request_id
    ImageId = Column(String(64))  # image 的 id ，在 image 创建成功后生成，必须
    SysSnapshotId = Column(String(64)) # 从指定的 snapshot 来创建镜像

class AliyunImageInstance(BaseModel):
    __tablename__ = 'image_instance_tbl'
    TaskCode = Column(String(200), primary_key=True) # vulcanus 的 request_id
    ImageId = Column(String(64))  # image 的 id ，在 image 创建成功后生成，必须
    InstanceId = Column(String(64)) # 实例 id

class AliyunImageDevMap(BaseModel):
    __tablename__ = 'image_dev_map_tbl'
    TaskCode = Column(String(200), primary_key=True) # vulcanus 的 request_id
    ImageId = Column(String(64))  # image 的 id ，在 image 创建成功后生成，必须
    SnapshotId = Column(String(64)) # 从指定的 snapshot 来创建镜像
    SnapshotSize = Integer # 指定快照对应磁盘的大小

# class AliyunImageImport(BaseModel):
#     __tablename__ = 'image_import_tbl'
#     TaskCode = Column(String(200), primary_key=True) # vulcanus 的 request_id


class AliyunImageSharePermission(BaseModel):
    __tablename__ = 'image_share_permission_tbl'
    TaskCode = Column(String(200), primary_key=True)  # vulcanus 的 request_id
    ImageId = Column(String(64))  # image 的 id ，在 image 创建成功后生成，必须
    AccountId = Column(String(64)) # 阿里云帐号 id

class AliyunImage(BaseModel):
    __tablename__ = 'image_tbl'
    TaskCode = Column(String(200), primary_key=True)  # vulcanus 的 request_id
    ImageId = Column(String(64)) # image 的 id ，在 image 创建成功后生成，必须
    RegionId = Column(String(64)) # region id ，必须
    ImageCreateType = Column(String(32)) # image 创建的方式 sys_snapshot_id, instance_id, device_mapping_ids, import, copy
    ImageName = Column(String(128)) # 镜像名字，最好传入
    ImageVersion = Column(String(40)) # 镜像版本, 最好传入
    Description = Column(String(256)) # 镜像的描述，最好传入

class AliyunImageSnapshotDBExecutor(CommonDBExecutor):

    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(AliyunImageSnapshotDBExecutor, self).__init__(db_url=db_url, table=AliyunImageSnapshot)

class AliyunImageInstanceDBExecutor(CommonDBExecutor):

    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(AliyunImageInstanceDBExecutor, self).__init__(db_url=db_url, table=AliyunImageInstance)

class AliyunImageDevMapDBExecutor(CommonDBExecutor):

    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(AliyunImageDevMapDBExecutor, self).__init__(db_url=db_url, table=AliyunImageDevMap)

class AliyunImageDBExecutor(CommonDBExecutor):

    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(AliyunImageDBExecutor, self).__init__(db_url=db_url, table=AliyunImage)

class AliyunImageSharePermissionDBExecutor(CommonDBExecutor):

    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(AliyunImageSharePermissionDBExecutor, self).__init__(db_url=db_url,
                                                                   table=AliyunImageSharePermission)
