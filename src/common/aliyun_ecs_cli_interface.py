#!/usr/bin/env python
# coding=utf-8
import random
import time
from datetime import datetime
import json
import argparse
from aliyun_ecs_operation_interface import ALiYunOperation
from log import init, debug, info, warn, error

ALIYUN_ECS_OPERATION_LOG = "/var/log/aliyun_ecs_operation.log"

def create_instance(args):
    print("a")

def delete_instance(args):
    print("c")

def desc_auto_renew_attr(args):
    pass

def describe_instances(args):
    pass

def describe_instances_status(args):
    pass

def describe_instance_vnc_url(args):
    pass

def describe_instance_usr_data(args):
    pass

def join_security_group(args):
    pass

def leave_security_group(args):
    pass

def modify_ins_attr(args):
    pass

def modify_ins_auto_res_time(args):
    pass

def modify_ins_auto_renew_time(args):
    pass

def modify_ins_vnc_pwd(args):
    pass

def modify_ins_vnc_attr(args):
    pass

def reboot_ins(args):
    pass

def renew_ins(args):
    pass

def start_ins(args):
    pass

def stop_ins(args):
    pass

def desc_ins_ram_role(args):
    pass

def attach_ram_role(args):
    pass

def detach_ram_role(args):
    pass

def create_disk(args):
    pass

def query_disk(args):
    pass

def attach_disk(args):
    pass

def detach_disk(args):
    pass

def modify_disk_attr(args):
    pass

def delete_disk(args):
    pass

def reinit_disk(args):
    pass

def reset_disk(args):
    pass

def rep_sys_disk(args):
    pass

def resize_disk(args):
    pass

def argument_parser():
    parser = argparse.ArgumentParser(description="Aliyun ECS Operation CLI.")
    parser.add_argument("-k", "--access-key", action="store", type=str, dest="ak",
                        help="Access key to do the operation on aliyun.", required=True)
    parser.add_argument("-s", "--access-secret", action="store", type=str, dest="secret",
                        help="Access secret to do the operation on aliyun.", required=True)
    parser.add_argument("-r", "--region-id", action="store", type=str, dest="region_id",
                        help="Instance or storage region id.", required=True)

    # create actions subcommand
    sparsers = parser.add_subparsers(title="Actions", description="Actions of ecs.",
                                    help="Action name, use -h after it for more details.")

    # create instance
    instance_tmp_name = "ecs-%d-%d" % (time.time(), random.randint(0, 10000))
    sparser_create_ins = sparsers.add_parser("create-instance", help="Create a instance on aliyun. Note: if you want to create instance with multiple data disks or tags, please use config file.")
    sparser_create_ins.set_defaults(func=create_instance)
    sparser_create_ins.add_argument("--image-id", action="store", type=str, dest="image_id",
                                    help="Image id for instance creating.", required=True)
    sparser_create_ins.add_argument("--specifications-id", action="store", type=str,
                                    dest="spec_id", help="Instance specifications id.", required=True)
    sparser_create_ins.add_argument("-z", "--zone-id", action="store", type=str, dest="zone_id",
                                    help="Instance zone id.", required=False, default="")
    sparser_create_ins.add_argument("--security-group-id", action="store", type=str,
                                    dest="security_group_id", help="Instance security group id.",
                                    required=False, default="")
    sparser_create_ins.add_argument("-n", "--name", action="store", type=str, dest="ins_name",
                                    help="Instance name.", required=False, default=instance_tmp_name)
    sparser_create_ins.add_argument("-D", "--description", action="store", type=str, dest="ins_desc",
                                    help="Instance description.", required=False,
                                    default="This is a instance named %s created at %s."
                                            % (instance_tmp_name, datetime.now().strftime("%Y,%m,%d - %H:%M:%S")))
    sparser_create_ins.add_argument("--internet-charge-type", action="store", type=str, dest="inet_charge_type",
                                    help="Internet charge type.", required=False, default="PayByTraffic")
    sparser_create_ins.add_argument("--internet-max-bandwidth-in", action="store", type=str, dest="inet_bandwidth_in",
                                    help="Internet max bandwidth in.", required=False, default="")
    sparser_create_ins.add_argument("--internet-max_bandwidth-out", action="store", type=str, dest="inet_bandwidth_out",
                                    help="Ineternet max bandwidth out.", required=False, default="")
    sparser_create_ins.add_argument("-H", "--hostname", action="store", type=str, dest="ins_hostname", help="Instance hostname.",
                                    required=False, default="sobey")
    sparser_create_ins.add_argument("-P", "--password", action="store", type=str, dest="ins_pass", help="Instance password.",
                                    required=False, default="newmedia")
    sparser_create_ins.add_argument("--io-optimized", action="store", type=str, dest="io_optimized", help="Instance io optimized flag.",
                                    required=False, default="optimized", nargs="?", const="optimized")
    sparser_create_ins.add_argument("--sys-disk-category", action="store", type=str, dest="sys_disk_cg", help="Instance system disk type.",
                                    required=False, default="")
    sparser_create_ins.add_argument("--sys-disk-size", action="store", type=int, dest="sys_disk_size", help="Instance system disk size.",
                                    required=False, default=40)
    sparser_create_ins.add_argument("--sys-disk-name", action="store", type=str, dest="sys_disk_name", help="Instance system disk name.",
                                    required=False, default="")
    sparser_create_ins.add_argument("--sys-disk-desc", action="store", type=str, dest="sys_disk_desc", help="Instance system disk description.",
                                    required=False, default="")
    sparser_create_ins.add_argument("--data-disk-category", action="store", type=str, dest="data_disk_cg",
                                    help="Instance data disk type.", required=False, default="")
    sparser_create_ins.add_argument("--data-disk-size", action="store", type=int, dest="data_disk_size",
                                    help="Instance data disk size.", required=False, default=10)
    sparser_create_ins.add_argument("--data-disk-name", action="store", type=str, dest="data_disk_name",
                                    help="Instance data disk name.", required=False, default="")
    sparser_create_ins.add_argument("--data-disk-desc", action="store", type=str, dest="data_disk_desc",
                                    help="Instance data disk description.",
                                    required=False, default="")
    sparser_create_ins.add_argument("--data-disk-snapshot-id", action="store", type=str, dest="data_disk_snapthotid",
                                    help="Create instance data disk with snapshot, the snapshot must be newer than 2013.7.15.",
                                    required=False, default="")
    sparser_create_ins.add_argument("--data-disk-del-with-ins", action="store", type=str, dest="data_disk_del_flag",
                                    help="If true the data disk will be delted by instance destory.",
                                    required=False, default="true", nargs="?", const="true")
    sparser_create_ins.add_argument("--vswitch-id", action="store", type=str, dest="vsw_id", help="Instance's virtual switch id of the related VPC.",
                                    required=False, default="")
    sparser_create_ins.add_argument("--instance-charge-type", action="store", type=str, dest="ins_charge_type",
                                    help="Instance charge type.", required=False, default="PostPaid")
    sparser_create_ins.add_argument("--period", action="store", type=int, dest="ins_period", help="Resource purchase time. Range: [1-9], 12, 24, 36 .",
                                    required=False, default=1)
    sparser_create_ins.add_argument("--auto-renew", action="store", type=str, dest="ins_auto_renew", help="Resource auto renew flag.",
                                    required=False, default="False", nargs="?", const="True")
    sparser_create_ins.add_argument("--auto-renew-period", action="store", type=int, dest="ins_auto_renew_period", help="Resource auto renew period. Range: [1|2|3|6|12].",
                                    required=False, default=1)
    sparser_create_ins.add_argument("--user-data", action="store", type=str, dest="user_data_path", help="User data path to read user data and set into instance.",
                                    required=False, default="")
    sparser_create_ins.add_argument("--client-token", action="store", type=str, dest="ins_client_token", help="Client flag when creating instance to avoid repeate instance created.",
                                    required=False, default="")
    sparser_create_ins.add_argument("--key-pair-name", action="store", type=str, dest="key_pair_name", help="SSH key pair for linux instance.",
                                    required=False, default="")
    sparser_create_ins.add_argument("--deployment-set-id", action="store", type=str, dest="deploy_set_id", help="Deploy id.", required=False, default="")
    sparser_create_ins.add_argument("--ram-role-name", action="store", type=str, dest="ins_ram_name", help="Instance ram role name.", required=False, default="")
    sparser_create_ins.add_argument("--security-enhancement-strategy", action="store", type=str, dest="sec_enhance_flag", help="Security enhancement strategy for images.",
                                    required=False, default="Deactive", nargs="?", const='Active')
    sparser_create_ins.add_argument("-t", "--tag", action="store", type=str, dest="ins_tag", help="Instance tag, format is <key>:<val>.", required=False, default="")
    sparser_create_ins.add_argument("-c", "--config", action="store", type=str, dest="ins_create_config", help="Config file for instance creation.", required=False, default="")

    # delete instance
    sparser_delete_ins = sparsers.add_parser("delete-instance", help="Delete a instance on aliyun.")
    sparser_delete_ins.set_defaults(func=delete_instance)
    sparser_delete_ins.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id", help="Instance id.", required=True)

    # describe instance auto renew attribute
    sparser_desc_auto_renew_attr = sparsers.add_parser("desc-ins-auto-renew-attribute", help="Describe a instance auto renew attribute.")
    sparser_desc_auto_renew_attr.set_defaults(func=desc_auto_renew_attr)
    sparser_desc_auto_renew_attr.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id", help="Instance id.", required=True)

    # describe instances
    sparser_query_ins = sparsers.add_parser("describe-instances", help="Query information of instances. Note: if you want to query instance with multiple tags, please use config file.")
    sparser_query_ins.set_defaults(func=describe_instances)
    sparser_query_ins.add_argument("--vpc-id", action="store", type=str, dest="vpc_id", help="VPC id.", required=False, default="")
    sparser_query_ins.add_argument("--vswitch-id", action="store", type=str, dest="vsw_id", help="VSwitch id.", required=False, default="")
    sparser_query_ins.add_argument("-z", "--zone", action="store", type=str, dest="zone", help="Zone id.", required=False, default="")
    sparser_query_ins.add_argument("--instance-ids", action="store", type=str, dest="instances", help='instances list, format is [“i-xx”, ”i-yy”, … “i-zz”]',
                                   required=False, default="")
    sparser_query_ins.add_argument("--specifications-id", action="store", type=str, dest="spec_id",
                                   help="Instance specifications id.", required=False, default="")
    sparser_query_ins.add_argument("--specifications-id-family", action="store", type=str, dest="spec_id_family",
                                   help="Instance specifications id family.", required=False, default="")
    sparser_query_ins.add_argument("--instance-net-type", action="store", type=str, dest="ins_net_type",
                                   help="Instance network type.", required=False, default="")
    sparser_query_ins.add_argument("--private-ip-addresses", action="store", type=str, dest="ins_pri_ips",
                                   help='Instance private ip addresses with network type vpc. Format is [“10.1.1.1”, ”10.1.2.1”, … “10.1.10.1”]',
                                   required=False, default="")
    sparser_query_ins.add_argument("--inner-ip-addresses", action="store", type=str, dest="ins_inner_ips",
                                   help='Instance private ip addresses with network type classic. Format is [“10.1.1.1”, ”10.1.2.1”, … “10.1.10.1”]',
                                   required=False, default="")
    sparser_query_ins.add_argument("--public-ip-addresses", action="store", type=str, dest="ins_pub_ips",
                                   help='Instance public ip addresses with network type classic. Format is [“42.1.1.1”, ”42.1.2.1”, … “42.1.10.1”]',
                                   required=False, default="")
    sparser_query_ins.add_argument("--security-group-id", action="store", type=str, dest="ins_security_gp",
                                   help="Instance security group id.", required=False, default="")
    sparser_query_ins.add_argument("--instance-charge-type", action="store", type=str, dest="ins_charge_type",
                                   help="Instance charge type [PrePaid|PostPaid].", required=False, default="")
    sparser_query_ins.add_argument("--inet-charge-type", action="store", type=str, dest="inet_charge_type",
                                   help="Internet charge type [PayByTraffic|PayByBandwidth].", required=False,
                                   default="")
    sparser_query_ins.add_argument("-n", "--name", action="store", type=str, dest="ins_name", help="Instance name",
                                   required=False, default="")
    sparser_query_ins.add_argument("--image-id", action="store", type=str, dest="img_id", help="Instance image id.",
                                   required=False, default="")
    # it seems that DeploymentSetId is not in the related sdk interface.
    sparser_query_ins.add_argument("--deployment-set-id", action="store", type=str, dest="deployment_set_id",
                                   help="Instance deployment set id.", required=False, default="")
    sparser_query_ins.add_argument("-S", "--status", action="store", type=str, dest="ins_status",
                                   help="Instance status [Running|Starting|Stopping|Stopped]", required=False,
                                   default="")
    sparser_query_ins.add_argument("--io-optimized", action="store", type=str, dest="ins_io_optimized",
                                   help="If the instance is io optimized [True|False].", required=False, default="")
    sparser_query_ins.add_argument("--page-number", action="store", type=int, dest="ins_list_page_nu",
                                   help="Instance status list page index number.", required=False, default="")
    sparser_query_ins.add_argument("--page-size", action="store", type=int, dest="ins_list_page_size",
                                   help="Instance status list page size.", required=False, default="")
    sparser_query_ins.add_argument("-t", "--tag", action="store", type=str, dest="ins_tag",
                                    help="Instance tag, format is <key>:<val>.", required=False, default="")
    sparser_query_ins.add_argument("-c", "--config", action="store", type=str, dest="ins_create_config",
                                    help="Config file for instance creation.", required=False, default="")

    # describe instance status
    sparser_query_ins_sta = sparsers.add_parser("describe-instances-status",
                                                help="Query information of instances status.")
    sparser_query_ins_sta.set_defaults(func=describe_instances_status)
    sparser_query_ins_sta.add_argument("-z", "--zone", action="store", type=str, dest="zone", help="Zone id.",
                                        required=False, default="")
    sparser_query_ins_sta.add_argument("--page-number", action="store", type=int, dest="ins_list_page_nu",
                                        help="Instance status list page index number.", required=False, default="")
    sparser_query_ins_sta.add_argument("--page-size", action="store", type=int, dest="ins_list_page_size",
                                        help="Instance status list page size.", required=False, default="")

    # describe instance vnc url
    sparser_query_ins_vnc_url = sparsers.add_parser("describe-instance-vnc-url",
                                                    help="Query information of instances VNC url.")
    sparser_query_ins_vnc_url.set_defaults(func=describe_instance_vnc_url)
    sparser_query_ins_vnc_url.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id", help="Instance id.", required=True)

    # describe instance user data
    sparser_query_ins_usr_data = sparsers.add_parser("describe-instance-usr-data",
                                                    help="Query information of instance user data.")
    sparser_query_ins_usr_data.set_defaults(func=describe_instance_usr_data)
    sparser_query_ins_usr_data.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                           help="Instance id.", required=True)

    # join security group
    sparser_join_security_gp = sparsers.add_parser("join-security-group",
                                                   help="Join to a security group.")
    sparser_join_security_gp.set_defaults(func=join_security_group)
    sparser_join_security_gp.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                           help="Instance id.", required=True)
    sparser_join_security_gp.add_argument("--security-group-id", action="store", type=str, dest="ins_security_gp",
                                        help="Instance security group id.", required=False, default="")

    # leave security group
    sparser_leave_security_gp = sparsers.add_parser("leave-security-group",
                                                   help="Leave from a security group.")
    sparser_leave_security_gp.set_defaults(func=leave_security_group)
    sparser_leave_security_gp.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                          help="Instance id.", required=True)
    sparser_leave_security_gp.add_argument("--security-group-id", action="store", type=str, dest="ins_security_gp",
                                          help="Instance security group id.", required=False, default="")

    # modify instance attribute
    sparser_modify_ins_attr = sparsers.add_parser("modify-instance-attr",
                                                  help="Modify instance attribute.")
    sparser_modify_ins_attr.set_defaults(func=modify_ins_attr)
    sparser_modify_ins_attr.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                          help="Instance id.", required=True)
    sparser_modify_ins_attr.add_argument("-n", "--name", action="store", type=str, dest="ins_name", help="Instance name",
                                        required=False, default="")
    sparser_modify_ins_attr.add_argument("-D", "--description", action="store", type=str, dest="ins_desc",
                                         help="Instance description.", required=False, default="")
    sparser_modify_ins_attr.add_argument("-P", "--password", action="store", type=str, dest="ins_pass", help="Instance password.",
                                        required=False, default="")
    sparser_modify_ins_attr.add_argument("-H", "--hostname", action="store", type=str, dest="ins_hostname", help="Instance hostname.",
                                        required=False, default="")
    sparser_modify_ins_attr.add_argument("--user-data", action="store", type=str, dest="user_data_path",
                                         help="User data path to read user data and set into instance.",
                                         required=False, default="")

    # modify instance auto release time
    sparser_mod_auto_release = sparsers.add_parser("modify-auto-release-time",
                                                   help="Modify instance auto release time. The charge type must be pay with traffic.")
    sparser_mod_auto_release.set_defaults(func=modify_ins_auto_res_time)
    sparser_mod_auto_release.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                          help="Instance id.", required=True)
    sparser_mod_auto_release.add_argument("--auto-release-time", action="store", type=str, dest="auto_res_time",
                                          help="Instance auto relase time, format is yyyy-MM-ddTHH:mm:ssZ",
                                          required=False, default="")

    # modify instance auto renew time
    sparser_mod_auto_renew = sparsers.add_parser("modify-auto-renew-time",
                                                 help="Modify instance auto renew time.")
    sparser_mod_auto_renew.set_defaults(func=modify_ins_auto_renew_time)
    sparser_mod_auto_renew.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                        help="Instance id.", required=True)
    sparser_mod_auto_renew.add_argument("-D", "--duration", action="store", type=int, dest="ins_duration",
                                        help="Instance renew duration [1|2|3|6|12].", required=False, default=1)
    sparser_mod_auto_renew.add_argument("--auto-renew", action="store", type=bool, dest="ins_auto_renew_flag",
                                        help="Instance auto renew enable flag.", required=False, default=False)

    # modify instance vnc password
    sparser_modify_vnc_pwd = sparsers.add_parser("modify-vnc-password",
                                                 help="Modify instance vnc password.")
    sparser_modify_vnc_pwd.set_defaults(func=modify_ins_vnc_pwd)
    sparser_modify_vnc_pwd.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                        help="Instance id.", required=True)
    sparser_modify_vnc_pwd.add_argument("-P", "--vnc-password", action="store", type=str, dest="vnc_pwd",
                                        help="VNC password.", required=True)

    # modify instance vnc attribute
    sparser_modify_vnc_attr = sparsers.add_parser("modify-vnc-attribute",
                                                  help="Modify instance vnc attribute.")
    sparser_modify_vnc_attr.set_defaults(func=modify_ins_vnc_attr)
    sparser_modify_vnc_attr.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                        help="Instance id.", required=True)
    sparser_modify_vnc_attr.add_argument("--vswitch-id", action="store", type=str, dest="vsw_id",
                                         help="Vswitch ID.", required=True)
    sparser_modify_vnc_attr.add_argument("--private-ip-address", action="store", type=str, dest="priv_ip",
                                         help="Private ip address.", required=False, default="")

    # reboot instance
    sparser_reboot = sparsers.add_parser("reboot-instance", help="Reboot instance.")
    sparser_reboot.set_defaults(func=reboot_ins)
    sparser_reboot.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                help="Instance id.", required=True)
    sparser_reboot.add_argument("--force-stop", action="store", type=str, dest="f_stop_flag",
                                help="Force shutdown enable flag.", required=False, defualt="")

    # renew instance
    sparser_renew = sparsers.add_parser("renew-instance", help="Renew instance.")
    sparser_renew.set_defaults(func=renew_ins)
    sparser_renew.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                help="Instance id.", required=True)
    sparser_renew.add_argument("--period", action="store", type=int, dest="renew_period",
                               help="Instance renew period [1-9|12|24|36].", required=True)
    sparser_renew.add_argument("--client-token", action="store", type=str, dest="cli_token",
                               help="Client token", required=False, default="")

    # start instance
    sparser_start = sparsers.add_parser("start-instance", help="Start instance.")
    sparser_start.set_defaults(func=start_ins)
    sparser_start.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                help="Instance id.", required=True)

    # stop instance
    sparser_stop = sparsers.add_parser("stop-instance", help="Stop instance.")
    sparser_stop.set_defaults(func=stop_ins)
    sparser_stop.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                help="Instance id.", required=True)
    sparser_stop.add_argument("--force-stop", action="store", type=str, dest="f_stop_flag",
                              help="Force stop.", required=False, default="")
    sparser_stop.add_argument("--confirm-stop", action="store", type=str, dest="c_stop_flag",
                              help="Confirm stop. If instance type is I1 this is required.",
                              required=False, default="")

    # describe instance ram role
    sparser_desc_ram_role = sparsers.add_parser("describe-instance-ram-role", help="Describe instance ram role.")
    sparser_desc_ram_role.set_defaults(func=desc_ins_ram_role)
    sparser_desc_ram_role.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                        help="Instance id.", required=True)

    # attach instance ram role
    sparser_attach_ram_role = sparsers.add_parser("attach-ram-role", help="Attach instance ram role.")
    sparser_attach_ram_role.set_defaults(func=attach_ram_role)
    sparser_attach_ram_role.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                        help="Instance id.", required=True)
    sparser_attach_ram_role.add_argument("--ram-role-name", action="store", type=str, dest="ram_role_name",
                                         help="Ram role name.", required=True)

    # detach instance ram role
    sparser_detach_ram_role = sparsers.add_parser("detach-ram-role", help="Detach instance ram role.")
    sparser_detach_ram_role.set_defaults(func=detach_ram_role)
    sparser_detach_ram_role.add_argument("--instance-ids", action="store", type=str, dest="ins_id",
                                        help="Instances id. Format is [“instanceId1”, “instanceId2”, “instanceId3”…]",
                                        required=True)
    sparser_detach_ram_role.add_argument("--ram-role-name", action="store", type=str, dest="ram_role_name",
                                         help="Ram role name.", required=True)

    # create disk
    disk_tmp_name = "disk-%d-%d" % (time.time(), random.randint(0, 10000))
    sparser_create_disk = sparsers.add_parser("create-disk", help="Create disk.")
    sparser_create_disk.set_defaults(func=create_disk)
    sparser_create_disk.add_argument("-z", "--zone", action="store", type=str, dest="zone", help="Zone id.", required=True)
    sparser_create_disk.add_argument("--disk-name", action="store", type=str, dest="disk_name", help="Disk name.",
                                     required=False, default=disk_tmp_name)
    sparser_create_disk.add_argument("-D", "--description", action="store", type=str, dest="disk_desc", help="Disk description",
                                     required=False, default="This is a disk named %s created at %s."
                                            % (disk_tmp_name, datetime.now().strftime("%Y,%m,%d - %H:%M:%S")))
    sparser_create_disk.add_argument("--disk-category", action="store", type=str, dest="disk_category",
                                     help="Disk category [cloud|cloud_efficiency|cloud_ssd].",
                                     required=False, default="cloud")
    sparser_create_disk.add_argument("-s", "--size", action="store", type=int, dest="disk_size",
                                     help="Disk size.", required=False, default=40)
    sparser_create_disk.add_argument("-S", "--snapshot-id", action="store", type=str, dest="snap_id",
                                     help="Use a snapshot for the disk, the argument size will be ignored.",
                                     required=False, default="")
    sparser_create_disk.add_argument("--client-token", action="store", type=str, dest="cli_token",
                                     help="Client token", required=False, default="")

    # query disk
    sparser_query_disk = sparsers.add_parser("query-disk", help="Query disk.")
    sparser_query_disk.set_defaults(func=query_disk)
    sparser_query_disk.add_argument("-z", "--zone", action="store", type=str, dest="zone", help="Zone id.", required=False, default="")
    sparser_query_disk.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                    help="Instance id.", required=False, default="")
    sparser_query_disk.add_argument("--disk-ids", action="store", type=str, dest="disks_id",
                                    help="Disks id. Format is [“d-xxxxxxxxx”, ”d-yyyyyyyyy”, … “d-zzzzzzzzz”]",
                                    required=False, default="")

    # attach disk
    sparser_attach_disk = sparsers.add_parser("attach-disk", help="Attach disk.")
    sparser_attach_disk.set_defaults(func=attach_disk)
    sparser_attach_disk.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                    help="Instance id.", required=True)
    sparser_attach_disk.add_argument("-D", "--disk-id", action="store", type=str, dest="disk_id",
                                    help="Disk id.", required=True)
    sparser_attach_disk.add_argument("--delete-with-instance", action="store", type=str, dest="del_with_ins",
                                    help="Enable delete with instance.", required=False, default="")

    # detach disk
    sparser_detach_disk = sparsers.add_parser("detach-disk", help="Detach disk.")
    sparser_detach_disk.set_defaults(func=detach_disk)
    sparser_detach_disk.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                    help="Instance id.", required=True)
    sparser_detach_disk.add_argument("-D", "--disk-id", action="store", type=str, dest="disk_id",
                                    help="Disk id.", required=True)

    # modify disk attribute
    sparser_mod_disk_attr = sparsers.add_parser("modify-disk-attribute", help="Modify disk attribute.")
    sparser_mod_disk_attr.set_defaults(func=modify_disk_attr)
    sparser_mod_disk_attr.add_argument("-d", "--disk-id", action="store", type=str, dest="disk_id",
                                        help="Disk id.", required=True)
    sparser_mod_disk_attr.add_argument("--disk-name", action="store", type=str, dest="disk_name",
                                        help="Disk name", required=False, default="")
    sparser_mod_disk_attr.add_argument("-D", "--description", action="store", type=str, dest="disk_desc",
                                       help="Disk description.", required=False, default="")
    sparser_mod_disk_attr.add_argument("--delete-with-instance", action="store", type=str, dest="del_with_ins",
                                        help="Enable delete with instance.", required=False, default="")
    sparser_mod_disk_attr.add_argument("--delete-auto-snapshot", action="store", type=str, dest="del_auto_snap",
                                       help="Delete auto snapshot when delete disk.", required=False, default="")
    sparser_mod_disk_attr.add_argument("--enable-auto-snapshot", action="store", type=str, dest="enable_auto_snap",
                                       help="Enable auto snapshot.", required=False, default="")

    # delete disk
    sparser_delete_disk = sparsers.add_parser("delete-disk", help="Delete disk.")
    sparser_delete_disk.set_defaults(func=delete_disk)
    sparser_delete_disk.add_argument("-D", "--disk-id", action="store", type=str, dest="disk_id",
                                    help="Disk id.", required=True)

    # reinit disk
    sparser_reinit_disk = sparsers.add_parser("reinit-disk", help="Reinit disk.")
    sparser_reinit_disk.set_defaults(func=reinit_disk)
    sparser_reinit_disk.add_argument("-D", "--disk-id", action="store", type=str, dest="disk_id",
                                    help="Disk id.", required=True)
    sparser_reinit_disk.add_argument("--security-enhancement", action="store", type=str, dest="secu_enhance",
                                     help="Enable security enhancement strategy.", required=False, default="")

    # reset disk
    sparser_reset_disk = sparsers.add_parser("reset-disk", help="Reset disk.")
    sparser_reset_disk.set_defaults(func=reset_disk)
    sparser_reset_disk.add_argument("-D", "--disk-id", action="store", type=str, dest="disk_id",
                                    help="Disk id.", required=True)
    sparser_reset_disk.add_argument("-S", "--snapshot-id", action="store", type=str, dest="snap_id",
                                     help="Use a snapshot for the disk, the argument size will be ignored.",
                                     required=True)

    # replace system disk
    sparser_rep_sys_disk = sparsers.add_parser("replace-sys-disk", help="Replace system disk.")
    sparser_rep_sys_disk.set_defaults(func=rep_sys_disk)
    sparser_rep_sys_disk.add_argument("-i", "--instance-id", action="store", type=str, dest="ins_id",
                                      help="Instance id.", required=True)
    sparser_rep_sys_disk.add_argument("--image-id", action="store", type=str, dest="img_id",
                                      help="Image id.", required=True)
    sparser_rep_sys_disk.add_argument("-s", "--sys-disk-size", action="store", type=str, dest="sys_disk_size",
                                      help="System disk size.", required=False, default="")
    sparser_rep_sys_disk.add_argument("--client-token", action="store", type=str, dest="cli_token",
                                      help="Client token", required=False, default="")
    sparser_rep_sys_disk.add_argument("--security-enhancement", action="store", type=str, dest="secu_enhance",
                                      help="Enable security enhancement strategy.", required=False, default="")

    # resize disk
    sparser_resize_disk = sparsers.add_parser("resize-disk", help="Resize disk.")
    sparser_resize_disk.set_defaults(func=resize_disk)
    sparser_resize_disk.add_argument("-D", "--disk-id", action="store", type=str, dest="disk_id",
                                     help="Disk id.", required=True)
    sparser_resize_disk.add_argument("-N", "--new-size", action="store", type=int, dest="new_size",
                                     help="New size.", required=True)
    sparser_resize_disk.add_argument("--client-token", action="store", type=str, dest="cli_token",
                                     help="Client token", required=False, default="")

    return parser.parse_args()

if __name__ == "__main__":
    init(logpath=ALIYUN_ECS_OPERATION_LOG)
    args = argument_parser()
    args.func(args)
