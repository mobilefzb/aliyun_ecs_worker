#!/usr/bin/env python
# coding=utf-8
import argparse
import signal
import sys
import time
import json
import traceback

from common.log import init, info, warn, debug, error
from common.rabbitmq_interface import RabbitmqService, pika
from common.utils import byteify
from common.setting import Cmop2ParametersAdapter, ConfigureDiscovery

