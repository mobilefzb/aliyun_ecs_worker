# coding=utf-8

from common.database import *

class AliyunDisk(BaseModel):
    __tablename__ = 'disk_tbl'
    TaskCode = Column(String(200), primary_key=True) # vulcanus 的 request_id
    InstanceId = Column(String(64)) # 实例的 id 在创建成功后生成，必须（创建失败不写入记录）
    RegionId = Column(String(64)) # 区域 id 必须
    ZoneId = Column(String(64)) # zone id ，非必须，从属于 RegionId
    DiskId = Column(String(64)) # disk id 必须
    DiskName = Column(String(128)) # 实例名字，最好传入
    Description = Column(String(256)) # 实例描述，最好传入
    DiskCategory = Column(String(32)) # 磁盘规格种类，最好传入
    DiskType = Column(String(32)) # 磁盘使用种类，system|data|all，最好传入
    DiskChargeType = Column(String(32)) # 磁盘付费方式，最好传入
    Size = Column(Integer) # 磁盘容量，最好传入
    DeleteWithInstance = Column(Boolean) # 磁盘随实例释放，非必须
    DeleteAutoSnapshot = Column(Boolean) # 删除磁盘时，是否删除快照，非必须
    EnableAutoSnapshot = Column(Boolean) # 磁盘是否执行自动快照策略，非必须
    Status = Column(String(32)) # 磁盘状态，主要用于状态标识，非必须
    Portable = Column(Boolean) # 磁盘是否支持卸载，非必须
    SecurityEnhancementStrategy = Column(String(32)) # 是否开启安全加固，非必须 ReplaceSystemDisk 会用到

class AliyunDiskDBExecutor(CommonDBExecutor):

    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(AliyunDiskDBExecutor, self).__init__(db_url=db_url, table=AliyunDisk)
