#!/usr/bin/env python
# coding=utf-8
import argparse
import signal
import sys
import time
import json

from common.log import init, info, warn, debug, error
from common.aliyun_product_operation_interface import ALiYunDiskOperation
from aliyun_disk_db_model import AliyunDiskDBExecutor

ALIYUN_ECS_OPERATION_LOG = "aliyun_disk.log"

def test_case_1():
    debug("===== Test disk create")
    c_res = aliy_op.CreateDisk(ZoneId="cn-hangzhou-e", DiskName="test_disk_fzb", Description="test the disk create.",
                       DiskCategory="cloud_efficiency", Size=66)
    debug(c_res)
    debug("===== Test disk create end")

def test_case_2():
    debug("===== Test disk delete")
    d_res = aliy_op.DeleteDisk(DiskId="d-bp13nu0s9j3xyxcvx9yi")
    debug(d_res)
    debug("===== Test disk delete end")

def test_case_3():
    debug("===== Test disk create")
    c_res = aliy_op.CreateDisk(ZoneId="cn-hangzhou-e", DiskName="test_disk_fzb_%d" % (time.time()),
                               Description="test the disk create at time %d." % (time.time()),
                               DiskCategory="cloud_efficiency", Size=66)
    debug(c_res)
    debug("===== Test disk create end")

    debug("===== Test disk query all")
    q_res = aliy_op.DescribeDisks()
    debug(q_res)
    debug("===== Test disk query end")

    disk_ids = []
    for info in q_res["Disks"]["Disk"]:
        if "test_disk_fzb" in info["DiskName"]:
            disk_ids.append(info["DiskId"])
    debug(disk_ids)

    debug("===== Test disk delete")
    for disk_id in disk_ids:
        d_res = aliy_op.DeleteDisk(DiskId=disk_id)
        debug(d_res)
    debug("===== Test disk delete end")

def test_case_4():
    aliy_db = AliyunDiskDBExecutor()
    # need to record attach relationship between instance and disk
    ret = aliy_db.query(DiskId="d-bp14lpnfv7wvtutr4hha")
    debug(ret)

def test_case_5():
    disks = ["d-bp15zdkhqiq18zxibony"]
    q_res = aliy_op.DescribeDisks(DiskIds=str(disks))
    debug(q_res)

def test_case_6():
    d_res = aliy_op.DeleteDisk(DiskId="d-2zeficfsnjplbe253fqb")
    debug(d_res)

def test_case_5_1():
    q_res = aliy_op.DescribeDisks(InstanceId="i-bp1a50pk1lwwpk97aezv")
    debug(q_res)

def test_case_7():
    DB_USER = "aliyun_computer"
    DB_PASS = "sobey@2016"
    DB_HOST = "172.17.0.4"
    DB_PORT = "3306"
    DB_NAME = "aliyun_computer_db"
    DATABASE_URI = 'mysql+mysqldb://%s:%s@%s:%s/%s?charset=utf8mb4' % (DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
    created_disk_info = {"TaskCode": "126", "InstanceId": "123", "RegionId": "cn-hangzhou", "ZoneId": "cn-hangzhou-b",
                         "DiskId": "456",
                         "DiskName": "测试磁盘创建", "Description": "测试磁盘创建", "DiskCategory": "cloud", "DiskType": "system",
                         "DiskChargeType": "PostPaid", "Size": 40, "DeleteWithInstance": True,
                         "DeleteAutoSnapshot": True, "EnableAutoSnapshot": False, "Status": "Available",
                         "Portable": False, "SecurityEnhancementStrategy": "Deactive"}
    # n_c_s = json.dumps(created_disk_info)
    # nc = json.loads(n_c_s)
    # print(nc)
    nc = created_disk_info
    aliy_db = AliyunDiskDBExecutor(db_url=DATABASE_URI)
    aliy_db.insert(**nc)

def test_case_8():
    DB_USER = "aliyun_computer"
    DB_PASS = "sobey@2016"
    DB_HOST = "172.17.0.4"
    DB_PORT = "3306"
    DB_NAME = "aliyun_computer_db"
    DATABASE_URI = 'mysql+mysqldb://%s:%s@%s:%s/%s?charset=utf8mb4' % (DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
    # n_c_s = json.dumps(created_disk_info)
    # nc = json.loads(n_c_s)
    # print(nc)
    aliy_db = AliyunDiskDBExecutor(db_url=DATABASE_URI)
    res=aliy_db.query(DiskId="456")
    print(res)

def test_case_9():
    debug("===== Detach Disk")
    res=aliy_op.DetachDisk(DiskId="d-bp1hojvn6vylocq7imdk", InstanceId="i-bp1atq3wv11uqbaahmxx")
    debug(res)
    debug("===== End of detach disk")

if __name__ == "__main__":
    init(logpath=ALIYUN_ECS_OPERATION_LOG)
    # ak = "LTAIr599QLK8bBhU"
    # sk = "HD8PRGYt1WIOJhqIbLF89bQoO8g4uK"
    region_id = "cn-hangzhou"
    ak = "LTAIliklfIP300rx"
    sk = "uJRyp2uUmG0TWaXbuS0oKhb5BALgFo"
    # region_id = "cn-beijing"
    global aliy_op
    aliy_op = ALiYunDiskOperation(ak, sk, region_id)
    # test_case_1()
    # test_case_2()
    # test_case_3()
    # test_case_4()
    # test_case_5()
    # test_case_6()
    # test_case_7()
    # test_case_8()
    # test_case_5_1()
    test_case_9()
