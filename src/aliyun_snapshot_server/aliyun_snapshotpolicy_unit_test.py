#!/usr/bin/env python
# coding=utf-8
import argparse
import signal
import sys
import time
import json

from common.log import init, info, warn, debug, error
from common.aliyun_product_operation_interface import ALiYunSnapshotOperation
from aliyun_snapshot_db_model import AliyunSnapshotDBExecutor, AliyunAutoSnapshotDBExecutor

ALIYUN_ECS_OPERATION_LOG = "aliyun_snapshot.log"

def test_case_1():
    debug("===== test autosnapshot delete")
    ret = aliy_op.DeleteAutoSnapshotPolicy(AutoSnapshotPolicyId="sp-bp1gyrpdck0b7mzurg42")
    debug(ret)

def test_case_2():
    debug("===== test snapshot create.")
    ret = aliy_op.CreateSnapshot(DiskId="d-bp1j6wytgbj6f025qhhc", RegionId="cn-hangzhou",
                                 SnapshotName="test_ecs_sys_disk_snapshot_01",
                                 Description="test ecs system disk snapshot create.")
    debug(ret)

def test_case_3():
    debug("===== test snapshot delete.")
    ret = aliy_op.DeleteSnapshot(SnapshotId="s-bp1eqd6o5b1w1u3ns5wb")
    debug(ret)

def test_case_4():
    debug("===== test autosnapshot apply")
    ret = aliy_op.ApplyAutoSnapshotPolicy(AutoSnapshotPolicyId="sp-bp12fhfpxhqmwtuxau35",
                                          DiskIds='["d-bp10sbbc0mkmczgi1dyx"]')
    debug(ret)

def test_case_5():
    debug("===== test cancle autosnapshot apply")
    ret = aliy_op.CancelAutoSnapshotPolicy(DiskIds='["d-bp10sbbc0mkmczgi1dyx"]')
    debug(ret)

def test_case_6():
    debug("===== test describe snapshot")
    ret = aliy_op.DescribeSnapshots(SnapshotIds='["s-bp1alst6rugcc8ru0u09"]')
    debug(ret)

def test_case_7():
    debug("==== test snapshot list delete")
    snapshot_list = ["s-bp19rye0fhrbm51nf73p", "s-bp14aqde7fbmaoa07pr8", "s-bp14aqde7fbmaoa07pr9", "s-bp10cwv8v7l5x4khowuk", "s-bp1e4dr61j7slq6luqcu", "s-bp1e4dr61j7slq6luqct", "s-bp10cwv8v7l5vr8pvj5r", "s-bp1et3m7jh6qj0tqxp5o", "s-bp152gjm3npxrk1yexq3", "s-bp152gjm3npxrk1yexq2", "s-bp1f5j6kl395xt1nrixr", "s-bp1iq1h2dcf9j10hfmfd", "s-bp1iq1h2dcf9j10hfmfc"]
    for snapshot in snapshot_list:
        ret = aliy_op.DeleteSnapshot(SnapshotId=snapshot)
        debug(ret)

if __name__ == "__main__":
    init(logpath=ALIYUN_ECS_OPERATION_LOG)
    # ak = "LTAIr599QLK8bBhU"
    # sk = "HD8PRGYt1WIOJhqIbLF89bQoO8g4uK"
    region_id = "cn-hangzhou"
    ak = "LTAIliklfIP300rx"
    sk = "uJRyp2uUmG0TWaXbuS0oKhb5BALgFo"
    # region_id = "cn-beijing"
    global aliy_op
    aliy_op = ALiYunSnapshotOperation(ak, sk, region_id)
    # test_case_1()
    # test_case_2()
    test_case_3()
    # test_case_4()
    # test_case_5()
    # test_case_6()
    # test_case_7()
