# coding=utf-8

from common.database import *

class AliyunSnapshot(BaseModel):
    __tablename__ = 'snapshot_tbl'
    TaskCode = Column(String(200), primary_key=True) # vulcanus 的 request_id
    SnapshotId = Column(String(64)) # snapshot 的 id ，在创建快照成功后生成，必须
    DiskId = Column(String(64)) # 磁盘的 id ，必须
    SnapshotName = Column(String(128)) # snapshot 名字，最好传入
    Description = Column(String(256)) # 快照描述，最好传入
    Status = Column(String(32)) # 快照状态 progressing | accomplished | failed | all ，非必须
    SnapshotType = Column(String(32)) # 快照类型，auto | user | all，非必须
    SourceDiskType = Column(String(32)) # 快照源磁盘的磁盘类型，System | Data，非必须
    Usage = Column(String(32)) # 有引用关系的资源类型，image | disk | image_disk | none，非必须

class AliyunAutoSnapshot(BaseModel):
    __tablename__ = 'autosnapshotpolicy_tbl'
    TaskCode = Column(String(200), primary_key=True)  # vulcanus 的 request_id
    AutoSnapshotPolicyId = Column(String(64)) # auto snapshot 的 id ，在创建自动快照后生成，必须
    DiskId = Column(String(64)) # 磁盘的 id ，非必须（只有应用了自动快照的磁盘才有映射关系）
    RegionId = Column(String(64)) # region id ，必须
    AutoSnapshotPolicyName = Column(String(128)) # 自动快照策略名字，必须
    TimePoints = Column(String(256)) # 自动快照的创建时间点格式[“0”, “1”, … “23”]，必须
    RepeatWeekdays = Column(String(256)) # 自动快照的重复日期，格式[ “1”，“2”…“7”]，必须
    RetentionDays = Column(Integer) # 指定自动快照的保存时间，单位为天-1：永久保存 1 ~ 65536：指定保存天数。

class AliyunSnapshotDBExecutor(CommonDBExecutor):

    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(AliyunSnapshotDBExecutor, self).__init__(db_url=db_url, table=AliyunSnapshot)

class AliyunAutoSnapshotDBExecutor(CommonDBExecutor):

    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(AliyunAutoSnapshotDBExecutor, self).__init__(db_url=db_url, table=AliyunAutoSnapshot)
