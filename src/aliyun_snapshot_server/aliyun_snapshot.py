#!/usr/bin/env python
# coding=utf-8
import argparse
import signal
import sys
import time
import json
import traceback

from common.log import init, info, warn, debug, error
from common.rabbitmq_interface import RabbitmqService, pika
from common.utils import byteify
from common.aliyun_product_operation_interface import ALiYunSnapshotOperation
import aliyun_snapshot_db_model
from aliyun_snapshot_db_model import AliyunAutoSnapshotDBExecutor, AliyunSnapshotDBExecutor
from common.setting import Cmop2ParametersAdapter, ConfigureDiscovery

ALIYUN_SERVER_LOG = "aliyun_snapshot.log"
ALIYUN_SERVER_CONF = "service.conf"
COMPUTE_EXCHANGE = "vulcanus-iaas-computer"
LOG_EXCHANGE = "vulcanus-iaas-log"
TASK_ROUTE_KEY = "vulcanus.task.create"

def _signal_handler(signum, frame):

    info("System exit.")
    sys.exit(0)

def task_report(req, repo, route_key):

    task_rep = {}
    rs = RabbitmqService()
    task_rep["taskCode"] = repo["taskCode"]
    task_rep["action"] = route_key
    task_rep["username"] = repo["username"]
    task_rep["status"] = repo["statusCode"]
    task_rep["requestData"] = req
    task_rep["responseData"] = json.dumps(repo)
    if isinstance(repo["infos"]["requestId"], list):
        for request_id in repo["infos"]["requestId"]:
            task_rep["requestId"] = request_id
            tr_body = json.dumps(task_rep)
            rs.publish(exchange=LOG_EXCHANGE, routing_key=TASK_ROUTE_KEY, message=tr_body)
    else:
        task_rep["requestId"] = repo["infos"]["requestId"]
        tr_body = json.dumps(task_rep)
        rs.publish(exchange=LOG_EXCHANGE, routing_key=TASK_ROUTE_KEY, message=tr_body)

def request_info_convert(request_info):

    # This is used for information keys convert
    if "ImageUUID" in request_info:
        request_info["ImageId"] = request_info["ImageUUID"]
    if "SecurityGroupUUID" in request_info:
        request_info["SecurityGroupId"] = request_info["SecurityGroupUUID"]
    if "DeploymentSetUUID" in request_info:
        request_info["DeploymentSetId"] = request_info["DeploymentSetUUID"]
    if "InstanceUUID" in request_info:
        request_info["InstanceId"] = request_info["InstanceUUID"]
    if "VpcUUID" in request_info:
        request_info["VpcId"] = request_info["VpcUUID"]
    if "VSwitchUUID" in request_info:
        request_info["VSwitchId"] = request_info["VSwitchUUID"]
    if "ZoneUUID" in request_info:
        request_info["ZoneId"] = request_info["ZoneUUID"]
    if "DiskUUID" in request_info:
        request_info["DiskId"] = request_info["DiskUUID"]
    if "SnapshotUUID" in request_info:
        request_info["SnapshotId"] = request_info["SnapshotUUID"]
    if "AutoSnapshotPolicyUUID" in request_info:
        request_info["AutoSnapshotPolicyId"] = request_info["AutoSnapshotPolicyUUID"]

    return request_info

def rpc_query_common(func):

    def wrapper(method, properties, body):
        info("[x] Received %r" % (body))
        info("[x] Begin to do %s ..." % (method.routing_key))
        # publish message to vulcanus-iaas-log access successful
        rs = RabbitmqService()
        req = json.loads(body, object_hook=byteify)

        # convert the data to current format.
        cmop2_adapter = Cmop2ParametersAdapter()
        request_info = cmop2_adapter.input_adapt(req)

        repo = func(**request_info)
        response_info = cmop2_adapter.output_adapt(repo)

        rs.publish(exchange='', routing_key=properties.reply_to,
                   properties=pika.BasicProperties(correlation_id=properties.correlation_id),
                   message=json.dumps(response_info))

        # publish message to vulcanus-iaas-log with task report
        task_report(body, response_info, "%s.result" % (method.routing_key))

    return wrapper

@rpc_query_common
def rpc_query_snapshots(ak, sk, region_id, **kwargs):
    aliy_op = ALiYunSnapshotOperation(ak, sk, region_id)
    repo = {}
    repo["Infos"] = aliy_op.DescribeSnapshots(**kwargs)
    return repo

@rpc_query_common
def rpc_query_autosnapshotpolicy(ak, sk, region_id, **kwargs):
    aliy_op = ALiYunSnapshotOperation(ak, sk, region_id)
    repo = {}
    repo["Infos"] = aliy_op.DescribeAutoSnapshotPolicyEx(**kwargs)
    return repo

def rpc_process(ch, method, properties, body):

    # debug("Get rpc call body : %r" % (body))
    ALIYUN_RPC_CALLBACK_DICT = {
        "vulcanus.snapshot.query.list": rpc_query_snapshots,
        "vulcanus.snapshot.policy.query": rpc_query_autosnapshotpolicy
    }
    # report to log the rpc is began
    ch.basic_publish(exchange=LOG_EXCHANGE, routing_key="result.access.successful",
                     body="Accept rpc request successful!")

    if method.routing_key in ALIYUN_RPC_CALLBACK_DICT:
        evfunc = ALIYUN_RPC_CALLBACK_DICT[method.routing_key]
        evfunc(method, properties, body)
        # publish message to vulcanus-iaas-log execute complete
        ch.basic_publish(exchange=LOG_EXCHANGE, routing_key="result.access.successful",
                         body="Process %s request complete!" % (method.routing_key))
    else:
        msg = "There is no callback to process request %s." % (method.routing_key)
        ch.basic_publish(exchange=LOG_EXCHANGE, routing_key="result.access.failure",
                         body=msg)

    ch.basic_ack(delivery_tag=method.delivery_tag)

def event_process_common(func):
    def wrapper(method, properties, body):
        info("[x] Received %r" % (body))
        info("[x] Begin to do %s ..." % (method.routing_key))
        # publish message to vulcanus-iaas-log access successful
        rs = RabbitmqService()

        # get basic info
        req = json.loads(body, object_hook=byteify)

        # convert the data to current format.
        cmop2_adapter = Cmop2ParametersAdapter()
        request_info = cmop2_adapter.input_adapt(req)
        repo = func(**request_info)
        response_info = cmop2_adapter.output_adapt(repo)

        msg = json.dumps(response_info)

        # publish message to vulcanus-iaas-computer with execute result
        result_key = "%s.result" % (method.routing_key)
        rs.publish(exchange=COMPUTE_EXCHANGE, routing_key=result_key,
                   message=msg)

        # publish message to vulcanus-iaas-log with task report
        task_report(body, response_info, result_key)

    return wrapper

@event_process_common
def event_create_snapshot(ak, sk, region_id, **kwargs):

    aliy_op = ALiYunSnapshotOperation(ak, sk, region_id)
    aliy_db = AliyunSnapshotDBExecutor()

    create_snapshot_info = {"TaskCode":"", "SnapshotId":"", "DiskId":"", "SnapshotName":"",
                            "Description":"", "Status":"", "SnapshotType":"", "SourceDiskType":"",
                            "Usage":""}
    rs = RabbitmqService()
    ret = aliy_op.CreateSnapshot(**kwargs)

    # check and wait the snapshot create complete.
    if ret["StatusCode"] == "200":
        for i in range(0, 360):
            s_ret = aliy_op.DescribeSnapshots(SnapshotIds='["%s"]' % (ret["SnapshotId"]))
            if s_ret["Snapshots"]["Snapshot"]:
                snapshot_info = s_ret["Snapshots"]["Snapshot"][0]
                if snapshot_info["Status"] == "accomplished":
                    break
                else:
                    debug("====== Check snapshot create status.")
                    debug("snapshot %s progress %s status %s." %
                          (snapshot_info["SnapshotId"], snapshot_info["Progress"],
                           snapshot_info["Status"]))
            else:
                debug("===== Waiting for snapshot creating...")
            rs.sleep(10)
        else:
            warn_msg = "Failed to check snapshot creation status due to timeout."
            warn(warn_msg)
            ret["Message"] = warn_msg

    repo = dict(kwargs, **ret)

    for k, v in repo.items():
        if k in create_snapshot_info:
            create_snapshot_info[k] = v

    if repo["StatusCode"] == "200":
        try:
            aliy_db.insert(**create_snapshot_info)
        except Exception as ex:
            error("Failed to insert new data to table for snapshot creation due to %s." % (ex))
            error(traceback.format_exc())
    else:
        error("Failed to create snapshot due to %s." % (repo["Message"]))

    return repo

@event_process_common
def event_create_autosnapshotpolicy(ak, sk, region_id, **kwargs):

    aliy_op = ALiYunSnapshotOperation(ak, sk, region_id)
    aliy_db = AliyunAutoSnapshotDBExecutor()

    create_autosnapshot_info = {"TaskCode":"", "AutoSnapshotPolicyId":"", "DiskId":"",
                                "RegionId":"", "AutoSnapshotPolicyName":"", "TimePoints":"",
                                "RepeatWeekdays":"", "RetentionDays":""}

    # convert repeatWeekdays and timePoints to aliyun format
    # conver 1,2,3 to "[\"1\",\"2\",\"3\"]"
    if "RepeatWeekdays" in kwargs:
        aliyun_repeat_week_days = json.dumps(list(map(lambda e : str(e), kwargs["RepeatWeekdays"].split(","))))
        kwargs["RepeatWeekdays"] = aliyun_repeat_week_days
    if "TimePoints" in kwargs:
        aliyun_time_points = json.dumps(list(map(lambda e : str(e), kwargs["TimePoints"].split(","))))
        kwargs["TimePoints"] = aliyun_time_points

    ret = aliy_op.CreateAutoSnapshotPolicy(**kwargs)
    repo = dict(kwargs, **ret)

    for k, v in repo.items():
        if k in create_autosnapshot_info:
            create_autosnapshot_info[k] = v

    if repo["StatusCode"] == "200":
        try:
            aliy_db.insert(**create_autosnapshot_info)
        except Exception as ex:
            error("Failed to insert new data to table for autosnapshotpolicy due to %s." % (ex))
            error(traceback.format_exc())
    else:
        error("Failed to create auto snapshot policy due to %s." % (repo["Message"]))

    return repo

@event_process_common
def event_delete_snapshot(ak, sk, region_id, **kwargs):

    aliy_op = ALiYunSnapshotOperation(ak, sk, region_id)
    ret = aliy_op.DeleteSnapshot(**kwargs)
    aliy_db = AliyunSnapshotDBExecutor()
    repo = dict(kwargs, **ret)

    if repo["StatusCode"] == "200":
        # delete ssh keypairs in the database
        # delete all of the information related to the key
        aliy_db.delete(SnapshotId=kwargs["SnapshotId"])
    else:
        error("Failed to delete snapshot due to %s." % (repo["Message"]))

    return repo

@event_process_common
def event_delete_autosnapshotpolicy(ak, sk, region_id, **kwargs):

    aliy_op = ALiYunSnapshotOperation(ak, sk, region_id)
    ret = aliy_op.DeleteAutoSnapshotPolicy(**kwargs)
    aliy_db = AliyunAutoSnapshotDBExecutor()
    repo = dict(kwargs, **ret)

    if repo["StatusCode"] == "200":
        # delete ssh keypairs in the database
        # delete all of the information related to the key
        aliy_db.delete(AutoSnapshotPolicyId=kwargs["AutoSnapshotPolicyId"])
    else:
        error("Failed to delete auto snapshot policy due to %s." % (repo["Message"]))

    return repo

def _event_modify_common(op_func, update_keys, db_func=None, **kwargs):
    repo = {}
    kwargs_keys = set(kwargs.keys())
    k_update_keys = update_keys & kwargs_keys
    if k_update_keys:
        update_dict = {}
        for update_key in update_keys:
            update_dict[update_key] = kwargs[update_key]
        ret = op_func(**kwargs)
        if "Filters" not in kwargs:
            kwargs["Filters"] = {}

            if "SnapshotId" in kwargs:
                kwargs["Filters"]["SnapshotId"] = kwargs["SnapshotId"]

            if "AutoSnapshotPolicyId" in kwargs:
                kwargs["Filters"]["AutoSnapshotPolicyId"] = kwargs["AutoSnapshotPolicyId"]

        if db_func:
            db_func(update_dict=update_dict, **kwargs["Filters"])

        repo = dict(kwargs, **ret)
        if "Infos" not in repo:
            repo["Infos"] = {}

        for k, v in update_dict.items():
            nk = "%s%s" % (k[0].lower(), k[1:])
            repo["Infos"][nk] = v
    else:
        repo["StatusCode"] = "Error"
        repo["Message"] = "Failed to get any parameters to update (%s)." % (op_func.__name__)
        repo["Infos"] = {}
        repo["Infos"]["requestId"] = ""
        repo = dict(repo, **kwargs)

    return repo

def _update_snapshot_result(update_dict, **kwargs):
    aliy_db_snapshot = AliyunSnapshotDBExecutor()
    aliy_db_snapshot.update(update_dict=update_dict, **kwargs)

@event_process_common
def event_modify_snapshot(ak, sk, region_id, **kwargs):
    aliy_op = ALiYunSnapshotOperation(ak, sk, region_id)
    update_keys = {"Description", "SnapshotName"}

    repo = _event_modify_common(aliy_op.ModifySnapshotAttribute, update_keys,
                                _update_snapshot_result, **kwargs)
    return repo

def _update_autosnapshotpolicy_result(update_dict, **kwargs):
    aliy_db_autosnapshot = AliyunAutoSnapshotDBExecutor()
    aliy_db_autosnapshot.update(update_dict=update_dict, **kwargs)

@event_process_common
def event_modify_autosnapshotpolicy(ak, sk, region_id, **kwargs):
    aliy_op = ALiYunSnapshotOperation(ak, sk, region_id)
    update_keys = {"AutoSnapshotPolicyId", "AutoSnapshotPolicyName", "TimePoints",
                   "RepeatWeekdays", "RetentionDays"}
    # convert repeatWeekdays and timePoints to aliyun format
    # conver 1,2,3 to "[\"1\",\"2\",\"3\"]"
    if "RepeatWeekdays" in kwargs:
        aliyun_repeat_week_days = json.dumps(list(map(lambda e: str(e), kwargs["RepeatWeekdays"].split(","))))
        kwargs["RepeatWeekdays"] = aliyun_repeat_week_days
    if "TimePoints" in kwargs:
        aliyun_time_points = json.dumps(list(map(lambda e: str(e), kwargs["TimePoints"].split(","))))
        kwargs["TimePoints"] = aliyun_time_points
    repo = _event_modify_common(aliy_op.ModifyAutoSnapshotPolicyEx, update_keys,
                                _update_autosnapshotpolicy_result, **kwargs)

    return repo

def _event_operation_common(op_func, **kwargs):
    repo = {}
    ret = op_func(**kwargs)
    repo = dict(kwargs, **ret)

    return repo

@event_process_common
def event_apply_autosnapshotpolicy(ak, sk, region_id, **kwargs):
    aliy_op = ALiYunSnapshotOperation(ak, sk, region_id)
    if "DiskIds" in kwargs:
        kwargs["DiskIds"] = json.dumps(kwargs["DiskIds"])
    repo = _event_operation_common(aliy_op.ApplyAutoSnapshotPolicy, **kwargs)
    return repo

@event_process_common
def event_cancel_autosnapshotpolicy(ak, sk, region_id, **kwargs):
    aliy_op = ALiYunSnapshotOperation(ak, sk, region_id)
    repo = _event_operation_common(aliy_op.CancelAutoSnapshotPolicy , **kwargs)
    return repo

def instance_event_process(ch, method, properties, body):

    ALIYUN_EVENT_CALLBACK_DICT = {
        "vulcanus.snapshot.create": event_create_snapshot,
        "vulcanus.snapshot.delete": event_delete_snapshot,
        "vulcanus.snapshot.modify": event_modify_snapshot,
        "vulcanus.snapshot.policy.create": event_create_autosnapshotpolicy,
        "vulcanus.snapshot.policy.delete": event_delete_autosnapshotpolicy,
        "vulcanus.snapshot.policy.modify": event_modify_autosnapshotpolicy,
        "vulcanus.snapshot.policy.operation.apply": event_apply_autosnapshotpolicy,
        "vulcanus.snapshot.policy.operation.cancel": event_cancel_autosnapshotpolicy
    }

    ch.basic_publish(exchange=LOG_EXCHANGE, routing_key="result.access.successful",
                     body="Accept %s request successful!" % (method.routing_key))

    if method.routing_key in ALIYUN_EVENT_CALLBACK_DICT:
        evfunc = ALIYUN_EVENT_CALLBACK_DICT[method.routing_key]
        evfunc(method, properties, body)
        # publish message to vulcanus-iaas-log execute complete
        ch.basic_publish(exchange=LOG_EXCHANGE, routing_key="result.access.successful",
                         body="Process %s request complete!" % (method.routing_key))
    else:
        msg = "There is no callback to process request %s." % (method.routing_key)
        ch.basic_publish(exchange=LOG_EXCHANGE, routing_key="result.access.failure",
                         body=msg)

    ch.basic_ack(delivery_tag=method.delivery_tag)

def start_rabbitmq_mode(args):

    host = args.rabbitmq_host
    port = args.rabbitmq_port
    user = args.rabbitmq_user
    password = args.rabbitmq_password
    rs = RabbitmqService(host, port, user, password)

    # create a channel, if one message can't be ack
    # the queue will be ignore
    rs.channel_create(prefetch_count=1)

    # # create a rpc server
    # rs.create_rpc_server(queue_name="Vulcanus.ecs.query", rpc_callback=rpc_process)

    # create two exchanges
    # declare exchange
    rs.exchange_declare(exchange=COMPUTE_EXCHANGE, type="topic", durable=True)
    rs.exchange_declare(exchange=LOG_EXCHANGE, type="fanout", durable=True)

    # bind queue
    # args = {"x-message-ttl": 60000}
    # related route keys
    route_keys = ["vulcanus.snapshot.create", "vulcanus.snapshot.delete", "vulcanus.snapshot.modify",
                  "vulcanus.snapshot.policy.create", "vulcanus.snapshot.policy.delete",
                  "vulcanus.snapshot.policy.modify", "vulcanus.snapshot.policy.operation.apply",
                  "vulcanus.snapshot.policy.operation.cancel"]
    q_name = "aliyun_snapshot_service"
    rs.queue_bind(COMPUTE_EXCHANGE, route_keys=route_keys,
                  queue_name=q_name, durable=True)
    rs.regist_consume(q_name, instance_event_process)

    # bind queue for rpc
    route_keys = ["vulcanus.snapshot.query.list", "vulcanus.snapshot.policy.query"]
    q_name = "aliyun_snapshot_rpc"
    rs.queue_bind(COMPUTE_EXCHANGE, route_keys=route_keys,
                  queue_name=q_name, durable=True)
    rs.regist_consume(q_name, rpc_process)

    # start consuming
    rs.start_consuming()
    signal.signal(signal.SIGINT, _signal_handler)
    rs.consuming_loop()

def argument_parser():
    parser = argparse.ArgumentParser(description="Aliyun ECS Operation CLI.")

    # create actions subcommand
    sparsers = parser.add_subparsers(title="Actions", description="Actions of the service.",
                                     help="Action name, use -h after it for more details.")
    # start rabbitmq server
    sparser_rabbitmq_conn = sparsers.add_parser("start-rabbitmq-to-connect",
                                                help="Start as a worker service for API Server events exchange. Note: if you have multiple events to confim and public plse use config file.")
    sparser_rabbitmq_conn.set_defaults(func=start_rabbitmq_mode)

    sparser_rabbitmq_conn.add_argument("-H", "--host", action="store", type=str, dest="rabbitmq_host",
                                       help="Rabbitmq server host to connect.", required=False, default="localhost")
    sparser_rabbitmq_conn.add_argument("-p", "--port", action="store", type=int, dest="rabbitmq_port",
                                       help="Rabbitmq server port to connect.", required=False, default=5672)
    sparser_rabbitmq_conn.add_argument("-u", "--user", action="store", type=str, dest="rabbitmq_user",
                                       help="Rabbitmq server user to connect.", required=False, default="guest")
    sparser_rabbitmq_conn.add_argument("-P", "--password", action="store", type=str, dest="rabbitmq_password",
                                       help="Rabbitmq server password to connect.", required=False, default="guest")
    sparser_rabbitmq_conn.add_argument("-c", "--config", action="store", type=str, dest="conf_path",
                                       help="Server config file.", required=False, default=ALIYUN_SERVER_CONF)
    sparser_rabbitmq_conn.add_argument("-d", "--daemon", action="store", type=bool, dest="daemon",
                                       help="Server daemonized flag.", required=False, default=False)
    sparser_rabbitmq_conn.add_argument("-l", "--log-path", action="store", type=str, dest="log_path",
                                       help="Server log path.", required=False, default=ALIYUN_SERVER_LOG)

    return parser.parse_args()

if __name__ == "__main__":
    args = argument_parser()
    init(logpath=args.log_path)
    cd = ConfigureDiscovery()
    cmop2_adapter = Cmop2ParametersAdapter()
    cd.regist_observer("input_mapping", cmop2_adapter.input_notify)
    cd.regist_observer("output_mapping", cmop2_adapter.output_notify)
    cd.regist_observer("db", aliyun_snapshot_db_model.notify)
    cd.start_observe_conf(conf_file=args.conf_path)
    cd.notify()
    args.func(args)
