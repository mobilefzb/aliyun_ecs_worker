#!/usr/bin/env python
# coding=utf-8
import os
import sys
import json
import traceback
import time

from mns.account import Account,AccountMeta
from mns.queue import *
from mns.topic import *
from mns.subscription import *
from mns.mns_exception import *

from log import init, debug, info, warn, error
from aliyun_product_operation_mns import *

ALIYUN_MNS_LOG = "aliyun_mns.log"

def test_case_1():
    q_n = "test-auto-queue-01"
    debug("===== Create queue %s" % (q_n))
    res = aliy_op.CreateQueue(QueueName=q_n)
    debug(res)
    debug("===== End create queue")

def test_case_2():
    q_n = "test-auto-queue-01"
    debug("===== Delete queue %s" % (q_n))
    res = aliy_op.DeleteQueue(QueueName=q_n)
    debug(res)
    debug("===== End delete queue")

def test_case_3():
    q_n = "test-auto-queue-01"
    msg = "test send message"
    debug("===== Send message %s" % (msg))
    res = aliy_op.SendMessage(QueueName=q_n, MessageBody=msg)
    debug(res)
    debug("===== End send message")

def test_case_4():
    q_n = "test-auto-queue-01"
    debug("===== Peek message")
    res = aliy_op.PeekMessage(QueueName=q_n)
    debug(res)
    debug("===== End peek message")

def test_case_5():
    q_n = "test-auto-queue-01"
    debug("===== Receive message")
    res = aliy_op.ReceiveMessage(QueueName=q_n)
    debug(res)
    debug("===== End receive message")
    return res

def test_case_6(**kwargs):
    q_n = "test-auto-queue-01"
    debug("===== Delete message")
    res = aliy_op.DeleteMessage(QueueName=q_n, ReceiptHandle=kwargs[s_receip_handle])
    debug(res)
    debug("===== End delete message")

def test_case_7(**kwargs):
    q_n = "test-auto-queue-01"
    debug("===== Change visibility")
    res = aliy_op.ChangeVisibility(QueueName=q_n, ReceiptHandle=kwargs[s_receip_handle],
                                   VisibilityTimeout=3000)
    debug(res)
    debug("===== End change visibility")
    return res

def test_case_8():
    q_n = "test-auto-queue-01"
    debug("===== Batch send message")
    msg_list = [{
        messagebody: "test message send batch 01"
    }, {
        messagebody: "test message send batch 02"
    }]
    res = aliy_op.BatchSendMessage(QueueName=q_n, Messages=msg_list)
    debug(res)
    debug("===== End send batch message")

def test_case_9():
    q_n = "test-auto-queue-01"
    debug("===== Batch peek message")
    res = aliy_op.BatchPeekMessage(QueueName=q_n, BatchSize=2)
    debug(res)
    debug("===== End peek batch message")

def test_case_10():
    q_n = "test-auto-queue-01"
    debug("===== Batch receive message")
    res = aliy_op.BatchReceiveMessage(QueueName=q_n, BatchSize=2)
    debug(res)
    debug("===== End receive batch message")
    return res

def test_case_11(recip_handle_list=[]):
    q_n = "test-auto-queue-01"
    debug("===== Batch delete message")
    # recip_handle_list = []
    res = aliy_op.BatchDeleteMessage(QueueName=q_n, ReceiptHandleList=recip_handle_list)
    debug(res)
    debug("===== End delete batch message")

def test_case_12():
    debug("===== List Queue")
    # recip_handle_list = []
    res = aliy_op.ListQueue()
    debug(res)
    debug("===== End list queue")

def test_case_13():
    debug("===== Create Topic")
    res = aliy_op.CreateTopic(TopicName="test-topic-01", MaximumMessageSize=1024,
                              LoggingEnabled=True)
    debug(res)
    debug("===== End topic creat")

def test_case_14():
    debug("===== List Topic")
    res = aliy_op.ListTopic()
    debug(res)
    debug("===== End list topic")

def test_case_15():
    debug("===== Set Topic Attribute")
    res = aliy_op.SetTopicAttribute(TopicName="test-topic-01", MaximumMessageSize=2048)
    debug(res)
    debug("===== End set topic attribute")

def test_case_16():
    debug("===== Get Topic Attribute")
    res = aliy_op.GetTopicAttribute(TopicName="test-topic-01")
    debug(res)
    debug("===== End get topic attribute")

def test_case_17():
    debug("===== Delete Topic")
    res = aliy_op.DeleteTopic(TopicName="test-topic-01")
    debug(res)
    debug("===== End delete topic")

def test_case_18():
    debug("===== Publish messages")
    res = aliy_op.PublishMessage(TopicName="test-topic-01", MessageBody="apple",
                                 MessageTag="fruit")
    debug(res)
    debug("===== End publis messages")

def test_case_19(queue_url):
    debug("===== Subscribe messages")
    res = aliy_op.Subscribe(TopicName="test-topic-01", SubName="test-subscription-01",
                            Endpoint=queue_url,
                            FilterTag="fruit", NotifyContentFormat="JSON")
    debug(res)
    debug("===== End subscribe messages")

def test_case_20():
    # 20171121 这个设置可以成功，但是实际没有任何作用，UI界面也不可以设置估计还没开放出来
    debug("===== Set Subscribe Attribute")
    res = aliy_op.SetSubAttribute(TopicName="test-topic-01", SubName="test-subscription-01",
                                  Endpoint="https://sobey.com/test", NotifyContentFormat="XML")
    debug(res)
    debug("===== End set subscribe attribute")

def test_case_21():
    debug("===== Get Subscribe Attribute")
    res = aliy_op.GetSubAttribute(TopicName="test-topic-01", SubName="test-subscription-01")
    debug(res)
    debug("===== End get subscribe attribute")

def test_case_22():
    debug("===== List Subscribe")
    res = aliy_op.ListSub(TopicName="test-topic-01")
    debug(res)
    debug("===== End list subscribe")

def test_case_23():
    debug("===== UnSubscribe")
    res = aliy_op.UnSubscribe(TopicName="test-topic-01", SubName="test-subscription-01")
    debug(res)
    debug("===== End unsubscribe")

if __name__ == '__main__':
    global aliy_op
    init(logpath=ALIYUN_MNS_LOG)
    ak = "LTAIliklfIP300rx"
    sk = "uJRyp2uUmG0TWaXbuS0oKhb5BALgFo"
    account_id = "1331197081468210"
    region_id = "cn-hangzhou"
    aliy_op = ALiYunMnsOperation(ak, sk, account_id, region_id, debug=True)

    # test_case_1()
    # test_case_2()
    # test_case_3()
    # time.sleep(1)
    # test_case_4()
    # res = test_case_5()
    # test_case_7(**res)
    # time.sleep(5)
    # test_case_6(**res)
    # time.sleep(5)
    # test_case_2()
    # test_case_8()
    # test_case_9()
    # res = test_case_10()
    # recip_handle_list = list(map(lambda msg_attr:msg_attr[s_receip_handle], res[msg_rsp_attrs]))
    # test_case_11(recip_handle_list)
    # test_case_12()
    # test_case_13()
    # test_case_14()
    # test_case_15()
    # test_case_16()
    test_case_17()
    # test_case_18()
    # test_case_19()
    # test_case_20()
    # test_case_21()
    # test_case_22()
    # test_case_23()