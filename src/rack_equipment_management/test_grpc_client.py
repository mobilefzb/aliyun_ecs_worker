# coding:utf8

from __future__ import print_function

import json
import grpc

import rack_equipment_management_pb2
import rack_equipment_management_pb2_grpc

def test_case0():
    # 新增 host-1 信息
    req_info = {"host_id":"host-1", "ip":"10.10.28.23"}
    req_info_str = json.dumps(req_info)
    response = stub.HostInfoPost(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case1():
    # 查询 host-1 的信息
    req_info = {"host_id":"host-1"}
    req_info_str = json.dumps(req_info)
    response = stub.HostInfoGet(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case2():
    # 查询所有 host 的信息
    req_info = {}
    req_info_str = json.dumps(req_info)
    response = stub.HostInfoGet(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case3():
    # 修改 host-1 参数
    req_info = {"host_id":"host-1", "ip":"10.10.28.29"}
    req_info_str = json.dumps(req_info)
    response = stub.HostInfoPut(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case4():
    # 删除 host-1
    req_info = {"host_id": "host-1"}
    req_info_str = json.dumps(req_info)
    response = stub.HostInfoDelete(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case5():
    # 新增 switch-1
    req_info = {"ip":"10.10.28.1","gateway_mac":"0000.0000.0002","remote_vtep":"2,3,4",
                "switch_port":"agg1,agg2,agg3,agg4,agg5","vtep":1,"description":"test sw",
                "vvlan_ip":2,"type":"TOR","manufacturer":"盛科","sw_id":"switch-1"}
    req_info_str = json.dumps(req_info)
    response = stub.SwitchInfoPost(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case6():
    # 查询 switch-1
    req_info = {"sw_id":"switch-1"}
    req_info_str = json.dumps(req_info)
    response = stub.SwitchInfoGet(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case7():
    # 查询所有 switch
    req_info = {}
    req_info_str = json.dumps(req_info)
    response = stub.SwitchInfoGet(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case8():
    # 修改 switch-1 参数
    req_info = {"sw_id":"switch-1", "manufacturer":"盛科3"}
    req_info_str = json.dumps(req_info)
    response = stub.SwitchInfoPut(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case9():
    # 删除 switch-1
    req_info = {"sw_id": "switch-1"}
    req_info_str = json.dumps(req_info)
    response = stub.SwitchInfoDelete(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case10():
    # 新增 rack-1
    req_info = {"rack_id":"rack-1", "rack_name":"test-rack"}
    req_info_str = json.dumps(req_info)
    response = stub.RackInfoPost(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case11():
    # 修改 rack-1
    req_info = {"rack_id":"rack-1", "rack_name":"test-rack-2"}
    req_info_str = json.dumps(req_info)
    response = stub.RackInfoPut(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case12():
    # 查询 rack-1
    req_info = {"rack_id":"rack-1"}
    req_info_str = json.dumps(req_info)
    response = stub.RackInfoGet(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case13():
    # 查询所有 rack
    req_info = {}
    req_info_str = json.dumps(req_info)
    response = stub.RackInfoGet(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case14():
    # 删除 rack-1
    req_info = {"rack_id":"rack-1"}
    req_info_str = json.dumps(req_info)
    response = stub.RackInfoDelete(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
    print("client received: status %d res %s." % (response.status_code, response.res))

def test_case15():
    # 新增 dc-1 一个连接数据
    req_info_list = [{"rack_id":"rack-1", "sw_id":"switch-1", "host_id":"host-1",
                        "virtualize_tec":"vmware", "dc_id":"dc-1"},
                     {"rack_id":"rack-1", "sw_id":"switch-1", "host_id":"host-2",
                        "virtualize_tec":"vmware", "dc_id":"dc-1"},
                     {"rack_id":"rack-1", "sw_id":"switch-1", "host_id":"host-3",
                        "virtualize_tec":"vmware", "dc_id":"dc-1"}]
    for req_info in req_info_list:
        req_info_str = json.dumps(req_info)
        response = stub.DCInfoPost(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
        print("client received: status %d res %s." % (response.status_code, response.res))

def test_case16():
    # 修改 dc-1 所有连接数据的 extend_info
    # 修改 dc-1 中不是 host-1 的 extend_info
    # 修改 dc-1 中是 host-1 的 extend_info
    req_info_list = [{"dc_id":"dc-1", "extend_info":"test extend info"},
                     {"dc_id":"dc-1", "extend_info":"test extend info67",
                      "host_id":"host-1", "condition":["!host_id"]},
                     {"dc_id":"dc-1", "extend_info":"test extend info445",
                      "host_id":"host-1", "condition":["host_id"]}]
    for req_info in req_info_list:
        req_info_str = json.dumps(req_info)
        response = stub.DCInfoPut(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
        print("client received: status %d res %s." % (response.status_code, response.res))

def test_case17():
    # 查询 dc-1 数据
    # 查询所有 dc 数据
    # 查询数据中心为 dc-1 交换机为 switch-1 宿主机为 host-1 的连接记录
    # 查询数据中心为 dc-1 交换机为 switch-1 宿主机不为 host-1 的连接记录
    req_info_list = [{"dc_id":"dc-1"}, {}, {"dc_id":"dc-1", "sw_id":"switch-1", "host_id":"host-1"},
                     {"dc_id":"dc-1", "sw_id":"switch-1", "!host_id":"host-1"}]
    for req_info in req_info_list:
        req_info_str = json.dumps(req_info)
        response = stub.DCInfoGet(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
        print("client received: status %d res %s." % (response.status_code, response.res))

def test_case18():
    # 删除 dc-1 下 host-1 的记录
    # 删除 dc-1 下非 host-2 的记录
    # 删除 dc-1 所有记录
    req_info_list = [{"dc_id":"dc-1", "host_id":"host-1"},
                     {"dc_id":"dc-1", "!host_id":"host-2"},
                     {"dc_id":"dc-1"}]
    for req_info in req_info_list:
        req_info_str = json.dumps(req_info)
        response = stub.DCInfoDelete(rack_equipment_management_pb2.ClientRequest(req=req_info_str))
        print("client received: status %d res %s." % (response.status_code, response.res))

if __name__ == '__main__':
    channel = grpc.insecure_channel('localhost:50051')
    global stub
    stub = rack_equipment_management_pb2_grpc.RackEquipmentManagementStub(channel)
    # test_case0()
    # test_case1()
    # test_case2()
    # test_case3()
    # test_case4()
    # test_case5()
    # test_case6()
    # test_case7()
    # test_case8()
    # test_case9()
    # test_case10()
    # test_case11()
    # test_case12()
    # test_case13()
    # test_case14()
    # test_case15()
    # test_case16()
    # test_case17()
    test_case18()
