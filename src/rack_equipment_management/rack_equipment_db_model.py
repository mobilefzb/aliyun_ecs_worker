# coding=utf-8

from common.database import *

class DCInfrastructure(BaseModel):
    __tablename__ = 'dc_infrastructure_tbl'
    OperationId = Column(Integer, primary_key=True, autoincrement=True)
    DataCenterId = Column(String(64))
    RackId = Column(String(64))
    SwitchId = Column(String(64))
    HostId = Column(String(64))
    VirtualizeTec = Column(String(32))
    ExtendInfo = Column(String(64))

class RackEquipment(BaseModel):
    __tablename__ = 'rack_eq_tbl'
    RackId = Column(String(64), primary_key=True)
    RackName = Column(String(128))

class SwitchEquipment(BaseModel):
    __tablename__ = 'sw_eq_tbl'
    SwitchId = Column(String(64), primary_key=True)
    Description = Column(Text(256))
    IpAddress = Column(String(32))
    GatewayMac = Column(String(32))
    RemoteVtep = Column(String(32))
    Manufacturer = Column(String(128))
    VVlanIp = Column(Integer)
    SwitchPort = Column(String(32))
    Type = Column(String(16))
    Vtep = Column(Integer)

class HostEquipment(BaseModel):
    __tablename__ = 'host_eq_tbl'
    # This host id must be same with the related
    # virtualize platform assigned id
    HostId = Column(String(64), primary_key=True)
    IpAddress = Column(String(32))

class HostEquipmentDBExecutor(CommonDBExecutor):
    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(HostEquipmentDBExecutor, self).__init__(db_url=db_url, table=HostEquipment)

class SwitchEquipmentDBExecutor(CommonDBExecutor):
    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(SwitchEquipmentDBExecutor, self).__init__(db_url=db_url, table=SwitchEquipment)

class RackEquipmentDBExecutor(CommonDBExecutor):
    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(RackEquipmentDBExecutor, self).__init__(db_url=db_url, table=RackEquipment)

class DCInfrastructureDBExecutor(CommonDBExecutor):
    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(DCInfrastructureDBExecutor, self).__init__(db_url=db_url, table=DCInfrastructure)
