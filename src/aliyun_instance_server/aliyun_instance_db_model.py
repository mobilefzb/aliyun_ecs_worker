# coding=utf-8

from common.database import *

class AliyunInstance(BaseModel):
    __tablename__ = 'instance_tbl'
    TaskCode = Column(String(200), primary_key=True) # vulcanus 的 request_id
    InstanceId = Column(String(64)) # 实例的 id 在创建成功后生成，必须（创建失败不写入记录）
    RegionId = Column(String(64)) # 区域 id 必须
    ZoneId = Column(String(64)) # zone id ，非必须，从属于 RegionId
    ImageId = Column(String(64)) # image id 必须
    InstanceType = Column(String(64)) # 实例的规格类型，非必须
    SecurityGroupId = Column(String(64)) # 实例所属安全组 id，最好传入
    InstanceName = Column(String(128)) # 实例名字，最好传入
    Description = Column(String(256)) # 实例描述，最好传入
    InternetChargeType = Column(String(32)) # 实例网络付费方式，最好传入
    InternetMaxBandwidthIn = Column(Integer) # 网络入带宽最大值[1,200]，非必须
    InternetMaxBandwidthOut = Column(Integer) # 网络出带宽最大值[0, 100]，非必须
    HostName = Column(String(30)) # instance 主机名字Windows平台最长为15字符其他（Linux等）平台最长为30字符，最好传入
    Password = Column(String(30)) # instance 主机密码，最好传入
    VncPassword = Column(String(6)) # instance vnc password 最长6位
    IoOptimized = Column(Boolean) # IO 优化使能，非必须
    SystemDiskCategory = Column(String(32)) # 系统盘的磁盘种类，非必须
    SystemDiskSize = Column(Integer) # 系统盘大小，GB为单位，非必须
    SystemDiskName = Column(String(128)) # 实例名字，最好传入
    SystemDiskDescription = Column(String(256)) # 系统盘描述
    VSwitchId = Column(String(64)) # vsw的id，必须
    PrivateIpAddress = Column(String(32)) # 实例私网ip地址，非必须
    PublicIpAddress = Column(String(32)) # 实例公网ip地址，非必须
    InstanceChargeType = Column(String(32)) # 实例付费方式，最好传入
    Period = Column(Integer) # 实例资源时长[1 - 9|12|24|36]，最好传入
    AutoRenew = Column(Boolean) # 是否自动续费，最好传入
    AutoRenewPeriod = Column(Integer) # 续费时长1|2|3|6|12，最好传入
    UserData = Column(Text(16348)) # 用户数据最大16kb，非必须
    DeploymentSetId = Column(String(32)) # 部署集id,非必须
    RamRoleName = Column(String(64)) # 实例角色名称，非必须

class AliyunInstanceDBExecutor(CommonDBExecutor):
    def __init__(self, db_url=''):
        if not db_url:
            db_url = get_db_uri()
        super(AliyunInstanceDBExecutor, self).__init__(db_url=db_url, table=AliyunInstance)
