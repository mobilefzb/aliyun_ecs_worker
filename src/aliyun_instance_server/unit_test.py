#!/usr/bin/env python
# coding=utf-8
import argparse
import signal
import sys
import time
import json

from common.log import init, info, warn, debug, error
from common.aliyun_product_operation_interface import ALiYunInstanceOperation
from aliyun_instance_db_model import AliyunInstanceDBExecutor

ALIYUN_ECS_OPERATION_LOG = "aliyun_instance.log"

def test_case_0():
    debug("=====describe region and zone")
    res = aliy_op.DescribeRegions()
    debug("regions: %s." % (res))
    res = aliy_op.DescribeZones()
    debug("zones: %s." % (res))

def test_case_1():
    # create instance
    debug("=====create instance")
    res = aliy_op.CreateInstance(ImageId="centos_7_03_64_40G_alibase_20170625.vhd", InstanceType="ecs.n1.tiny",
                                 ZoneId="cn-hangzhou-e")  # ,
    # SecurityGroupId="sg-bp1jb1dt5pdv12oenc6h", InstanceName="test-ecs-01",ZoneId="cn-hangzhou-e",
    # InternetChargeType="PayByTraffic", AutoRenew=False, InternetMaxBandwidthIn=1,
    # InternetMaxBandwidthOut=1, HostName="fzb_test_centos", Password="fzb12#$%",
    # VSwitchId="vsw-bp1k3lwt03yps72i5n5a8", SystemDiskSize=40, SystemDiskCategory="cloud",
    # SystemDiskName="ecs-01-sys-disk", SystemDiskDescription="test", InstanceChargeType="PostPaid")
    debug("result is %s." % (res))

def test_case_2():
    # query instance
    debug("=====query instance")
    res = aliy_op.DescribeInstances(ZoneId="cn-hangzhou-b")
    debug("result is %s." % (res))

def test_case_3():
    # delete instance
    debug("=====delete instance")
    res = aliy_op.DeleteInstance(InstanceId="i-uf6hbl3ph8t95m4ulqvv", Force=True)
    debug("result is %s." % (res))

def test_case_3_1():
    # query instance status
    debug("=====query instance status")
    qs_res = aliy_op.DescribeInstanceStatus(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (qs_res))

def test_case_4():
    # create instance
    debug("=====create instance")
    c_res = aliy_op.CreateInstance(ImageId="centos_7_03_64_40G_alibase_20170625.vhd",
                                 InstanceType="ecs.n1.tiny", ZoneId="cn-hangzhou-e")
    debug("result is %s." % (c_res))

    # query instance
    debug("=====query instance")
    q_res = aliy_op.DescribeInstances(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (q_res))

    # query instance status
    debug("=====query instance status")
    qs_res = aliy_op.DescribeInstanceStatus(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (qs_res))

    # time.sleep(10)
    # delete instance
    c_res_json = json.loads(c_res)
    debug("=====delete instance")
    for t in range(10):
        d_res = aliy_op.DeleteInstance(InstanceId=c_res_json["InstanceId"], Force=True)
        if d_res:
            break
        else:
            debug("Try again ... ")
            time.sleep(10)
    debug("result is %s." % (d_res))

def test_case_5():
    # create instance
    debug("=====create instance")
    # 如果是在vpc下建立ecs，那么VSwitchId是必要的，否则会认为是经典网络
    c_res = aliy_op.CreateInstance(ImageId="centos_7_03_64_40G_alibase_20170625.vhd",
                                 InstanceType="ecs.n1.tiny", ZoneId="cn-hangzhou-e",
                                 VSwitchId="vsw-bp1k3lwt03yps72i5n5a8",
                                 SecurityGroupId="sg-bp1jb1dt5pdv12oenc6h")
    debug("result is %s." % (c_res))

    if not c_res:
        return

    # query instance
    debug("=====query instance")
    q_res = aliy_op.DescribeInstances(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (q_res))

    # query instance status
    debug("=====query instance status")
    qs_res = aliy_op.DescribeInstanceStatus(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (qs_res))

    # time.sleep(10)
    # delete instance
    c_res_json = json.loads(c_res)
    debug("=====delete instance")
    d_res = None
    for t in range(10):
        d_res = aliy_op.DeleteInstance(InstanceId=c_res_json["InstanceId"], Force=True)
        if d_res:
            break
        else:
            debug("Try again %d ... " % (t))
            time.sleep(10)
    debug("result is %s." % (d_res))

def test_case_6():
    # create instance
    debug("=====create instance")
    c_res = aliy_op.CreateInstance(ImageId="centos_7_03_64_40G_alibase_20170625.vhd",
                                 InstanceType="ecs.n1.tiny", ZoneId="cn-hangzhou-e",
                                 VSwitchId="vsw-bp1k3lwt03yps72i5n5a8",
                                 SecurityGroupId="sg-bp1jb1dt5pdv12oenc6h",
                                 InstanceName="test-ecs-01", Description="test ecs build.")
    debug("result is %s." % (c_res))

    if not c_res:
        return

    # query instance
    debug("=====query instance")
    q_res = aliy_op.DescribeInstances(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (q_res))

    # query instance status
    debug("=====query instance status")
    qs_res = aliy_op.DescribeInstanceStatus(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (qs_res))

    # time.sleep(10)
    # delete instance
    c_res_json = json.loads(c_res)
    debug("=====delete instance")
    d_res = None
    for t in range(10):
        d_res = aliy_op.DeleteInstance(InstanceId=c_res_json["InstanceId"], Force=True)
        if d_res:
            break
        else:
            debug("Try again %d ... " % (t))
            time.sleep(10)
    debug("result is %s." % (d_res))

def test_case_7():
    # create instance
    debug("=====create instance")
    c_res = aliy_op.CreateInstance(ImageId="centos_7_03_64_40G_alibase_20170625.vhd",
                                 InstanceType="ecs.n1.tiny", ZoneId="cn-hangzhou-e",
                                 VSwitchId="vsw-bp1k3lwt03yps72i5n5a8",
                                 SecurityGroupId="sg-bp1jb1dt5pdv12oenc6h",
                                 InstanceName="test-ecs-01", Description="test ecs build.",
                                 InternetChargeType="PayByTraffic", InstanceChargeType="PostPaid")
    debug("result is %s." % (c_res))

    if not c_res:
        return

    # query instance
    debug("=====query instance")
    q_res = aliy_op.DescribeInstances(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (q_res))

    # query instance status
    debug("=====query instance status")
    qs_res = aliy_op.DescribeInstanceStatus(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (qs_res))

    # time.sleep(10)
    # delete instance
    c_res_json = json.loads(c_res)
    debug("=====delete instance")
    d_res = None
    for t in range(10):
        d_res = aliy_op.DeleteInstance(InstanceId=c_res_json["InstanceId"], Force=True)
        if d_res:
            break
        else:
            debug("Try again %d ... " % (t))
            time.sleep(10)
    debug("result is %s." % (d_res))

def test_case_8():
    # create instance
    debug("=====create instance")
    c_res = aliy_op.CreateInstance(ImageId="centos_7_03_64_40G_alibase_20170625.vhd",
                                 InstanceType="ecs.n1.tiny", ZoneId="cn-hangzhou-e",
                                 VSwitchId="vsw-bp1k3lwt03yps72i5n5a8",
                                 SecurityGroupId="sg-bp1jb1dt5pdv12oenc6h",
                                 InstanceName="test-ecs-01", Description="test ecs build.",
                                 InternetChargeType="PayByTraffic", InstanceChargeType="PostPaid",
                                 InternetMaxBandwidthIn="1", InternetMaxBandwidthOut="1")
    debug("result is %s." % (c_res))

    if not c_res:
        return

    # query instance
    debug("=====query instance")
    q_res = aliy_op.DescribeInstances(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (q_res))

    # query instance status
    debug("=====query instance status")
    qs_res = aliy_op.DescribeInstanceStatus(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (qs_res))

    # time.sleep(10)
    # delete instance
    c_res_json = json.loads(c_res)
    debug("=====delete instance")
    d_res = None
    for t in range(10):
        d_res = aliy_op.DeleteInstance(InstanceId=c_res_json["InstanceId"], Force=True)
        if d_res:
            break
        else:
            debug("Try again %d ... " % (t))
            time.sleep(10)
    debug("result is %s." % (d_res))

def test_case_9():
    # create instance
    debug("=====create instance")
    c_res = aliy_op.CreateInstance(ImageId="centos_7_03_64_40G_alibase_20170625.vhd",
                                 InstanceType="ecs.n1.tiny", ZoneId="cn-hangzhou-e",
                                 VSwitchId="vsw-bp1k3lwt03yps72i5n5a8",
                                 SecurityGroupId="sg-bp1jb1dt5pdv12oenc6h",
                                 InstanceName="test-ecs-01", Description="test ecs build.",
                                 InternetChargeType="PayByTraffic", InstanceChargeType="PostPaid",
                                 InternetMaxBandwidthIn="1", InternetMaxBandwidthOut="1",
                                 HostName="fzb-test", Password="hxfzb12#$")
    debug("result is %s." % (c_res))

    if not c_res:
        return

    # query instance
    debug("=====query instance")
    q_res = aliy_op.DescribeInstances(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (q_res))

    # query instance status
    debug("=====query instance status")
    qs_res = aliy_op.DescribeInstanceStatus(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (qs_res))

    # time.sleep(10)
    # delete instance
    c_res_json = json.loads(c_res)
    debug("=====delete instance")
    d_res = None
    for t in range(10):
        d_res = aliy_op.DeleteInstance(InstanceId=c_res_json["InstanceId"], Force=True)
        if d_res:
            break
        else:
            debug("Try again %d ... " % (t))
            time.sleep(10)
    debug("result is %s." % (d_res))

def test_case_10():
    # create instance
    # ecs.n1.tiny does not support IoOptimized
    debug("=====create instance")
    c_res = aliy_op.CreateInstance(ImageId="centos_7_03_64_40G_alibase_20170625.vhd",
                                 InstanceType="ecs.n1.tiny", ZoneId="cn-hangzhou-e",
                                 VSwitchId="vsw-bp1k3lwt03yps72i5n5a8",
                                 SecurityGroupId="sg-bp1jb1dt5pdv12oenc6h",
                                 InstanceName="test-ecs-01", Description="test ecs build.",
                                 InternetChargeType="PayByTraffic", InstanceChargeType="PostPaid",
                                 InternetMaxBandwidthIn="1", InternetMaxBandwidthOut="1",
                                 HostName="fzb-test", Password="hxfzb12#$")
    debug("result is %s." % (c_res))

    if not c_res:
        return

    # query instance
    debug("=====query instance")
    q_res = aliy_op.DescribeInstances(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (q_res))

    # query instance status
    debug("=====query instance status")
    qs_res = aliy_op.DescribeInstanceStatus(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (qs_res))

    # time.sleep(10)
    # delete instance
    c_res_json = json.loads(c_res)
    debug("=====delete instance")
    d_res = None
    for t in range(10):
        d_res = aliy_op.DeleteInstance(InstanceId=c_res_json["InstanceId"], Force=True)
        if d_res:
            break
        else:
            debug("Try again %d ... " % (t))
            time.sleep(10)
    debug("result is %s." % (d_res))

def test_case_11():
    # create instance
    # ecs.n1.tiny does not support IoOptimized
    debug("=====create instance")
    c_res = aliy_op.CreateInstance(ImageId="centos_7_03_64_40G_alibase_20170625.vhd",
                                 InstanceType="ecs.n1.small", ZoneId="cn-hangzhou-e",
                                 VSwitchId="vsw-bp1k3lwt03yps72i5n5a8",
                                 SecurityGroupId="sg-bp1jb1dt5pdv12oenc6h",
                                 InstanceName="test-ecs-01", Description="test ecs build.",
                                 InternetChargeType="PayByTraffic", InstanceChargeType="PostPaid",
                                 InternetMaxBandwidthIn="1", InternetMaxBandwidthOut="1",
                                 HostName="fzb-test", Password="hxfzb12#$", SystemDiskCategory="cloud_ssd",
                                 SystemDiskSize=50, SystemDiskName="fzb-test-disk", SystemDiskDescription="for test.")
    debug("result is %s." % (c_res))

    if not c_res:
        return

    # query instance
    debug("=====query instance")
    q_res = aliy_op.DescribeInstances(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (q_res))

    # query instance status
    debug("=====query instance status")
    qs_res = aliy_op.DescribeInstanceStatus(ZoneId="cn-hangzhou-e")
    debug("result is %s." % (qs_res))

    # time.sleep(10)
    # delete instance
    c_res_json = json.loads(c_res)
    debug("=====delete instance")
    d_res = None
    for t in range(10):
        d_res = aliy_op.DeleteInstance(InstanceId=c_res_json["InstanceId"], Force=True)
        if d_res:
            break
        else:
            debug("Try again %d ... " % (t))
            time.sleep(10)
    debug("result is %s." % (d_res))

def test_case_12():
    # create instance
    # ecs.n1.tiny does not support IoOptimized
    # 如果不指定安全组id默认会选择该区域下匹配网络的第一个安全组
    # 如果没有默认安全组则会自动新建一个
    debug("=====create instance")
    c_res = aliy_op.CreateInstance(ImageId="centos_7_03_64_40G_alibase_20170625.vhd",
                                   InstanceType="ecs.n1.small", ZoneId="cn-hangzhou-e",
                                   VSwitchId="vsw-bp1k3lwt03yps72i5n5a8",
                                   SecurityGroupId="sg-bp1jb1dt5pdv12oenc6h",
                                   InstanceName="test-ecs-03", Description="test ecs build.",
                                   InternetChargeType="PayByTraffic", InstanceChargeType="PostPaid",
                                   InternetMaxBandwidthIn=1, InternetMaxBandwidthOut=1,
                                   HostName="fzb-test", Password="hxfzb12#$", SystemDiskCategory="cloud_efficiency",
                                   SystemDiskSize=50, SystemDiskName="fzb-test-disk", SystemDiskDescription="for test.")
    debug("result is %s." % (c_res))

    if not c_res:
        return

    # query instance status
    debug("=====query instance status")
    # qs_res = aliy_op.DescribeInstanceStatus(ZoneId="cn-hangzhou-e")
    for i in range(1, 30):
        qs_res = aliy_op.DescribeInstances(InstanceIds='["%s"]' % (c_res["InstanceId"]))
        # debug("result is %s." % (qs_res))
        debug("Status is %s." % (qs_res["Instances"]["Instance"][0]["Status"]))
        # flag = False
        # for i in range(10):
        #     for ins in qs_res["InstanceStatuses"]["InstanceStatus"]:
        #         if ins["InstanceId"] == c_res["InstanceId"]:
        #             debug("status %s" % (ins))
        #             if ins["Status"] == "Pending":
        #                 flag = True
        #                 break
        #     if flag:
        #         break
        #     time.sleep(5)
        if qs_res["Instances"]["Instance"][0]["Status"] != "Pending":
            break
        time.sleep(5)

    # start instance
    debug("=====start instance")
    s_res = aliy_op.StartInstance(InstanceId=c_res["InstanceId"])
    debug("start ret %s" % (s_res))
    for i in range(10):
        if s_res["StatusCode"] == "200":
            break
        else:
            time.sleep(5)
            s_res = aliy_op.StartInstance(InstanceId=c_res["InstanceId"])
            debug("start ret %s" % (s_res))


    # query instance
    debug("=====query instance")
    q_res = aliy_op.DescribeInstances(ZoneId="cn-hangzhou-e", InstanceName="test-ecs-01")
    debug("result is %s." % (q_res))
    for i in range(1, 30):
        qs_res = aliy_op.DescribeInstances(InstanceIds='["%s"]' % (c_res["InstanceId"]))
        # debug("result is %s." % (qs_res))
        debug("Status is %s." % (qs_res["Instances"]["Instance"][0]["Status"]))
        # flag = False
        # for i in range(10):
        #     for ins in qs_res["InstanceStatuses"]["InstanceStatus"]:
        #         if ins["InstanceId"] == c_res["InstanceId"]:
        #             debug("status %s" % (ins))
        #             if ins["Status"] == "Pending":
        #                 flag = True
        #                 break
        #     if flag:
        #         break
        #     time.sleep(5)
        if qs_res["Instances"]["Instance"][0]["Status"] == "Running":
            break
        time.sleep(5)
    debug("===== instance started.")
    # for i in range(10):
    #     for ins in q_res["Instances"]["Instance"]:
    #         if ins["InstanceId"] == c_res["InstanceId"]:
    #             if ins["VpcAttributes"]["PrivateIpAddress"]["IpAddress"]:
    #                 debug("Get private ip: %s" % (ins["VpcAttributes"]["PrivateIpAddress"]["IpAddress"]))
    #                 break
    #     time.sleep(10)
    # else:
    #     debug("Failed to get private ip of the instance %s." % (c_res["InstanceId"]))
    #
    # time.sleep(10)
    # delete instance
    # c_res_json = c_res
    # debug("=====delete instance")
    # d_res = None
    # for t in range(10):
    #     d_res = aliy_op.DeleteInstance(InstanceId=c_res_json["InstanceId"], Force=True)
    #     if d_res["StatusCode"] == "200":
    #         break
    #     else:
    #         debug("Try again %d ... " % (t))
    #         time.sleep(10)
    # debug("result is %s." % (d_res))

def test_case_13():
    # modify ecs vpc attribute
    debug("=====modify ecs vpc attribute")
    m_res = aliy_op.ModifyInstanceVpcAttribute(InstanceId="", VSwitchId="", PrivateIpAddress="")
    debug("result is %s." % (m_res))

def test_case_14():
    # restart ecs
    pass

def test_case_15():
    # test database
    # docker run --rm --name workflowdb --env-file mysql.env.list mysql
    DB_USER = "aliyun_ecs"
    DB_PASS = "sobey@2016"
    DB_HOST = "172.17.0.2"
    DB_PORT = "3306"
    DB_NAME = "aliyun_ecs_db"
    DATABASE_URI = 'mysql+mysqldb://%s:%s@%s:%s/%s' % (DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
    aliy_db = AliyunInstanceDBExecutor(db_url=DATABASE_URI)
    aliy_db.insert(InstanceId="i-bp19l585e4itj1j3i5zk", RegionId="cn-hangzhou",
                   ImageId="centos_7_03_64_40G_alibase_20170625.vhd", TaskCode="v-op-01",
                   InstanceType="ecs.n1.tiny", ZoneId="cn-hangzhou-e",
                   VSwitchId="vsw-bp1k3lwt03yps72i5n5a8",
                   SecurityGroupId="sg-bp1jb1dt5pdv12oenc6h",
                   InstanceName="test-ecs-01", Description="test ecs build.",
                   InternetChargeType="PayByTraffic", InstanceChargeType="PostPaid",
                   InternetMaxBandwidthIn=1, InternetMaxBandwidthOut=1,
                   HostName="fzb-test", Password="hxfzb12#$", SystemDiskCategory="cloud_efficiency",
                   SystemDiskSize=50, SystemDiskName="fzb-test-disk", SystemDiskDescription="for test.")
    query_res = aliy_db.query()
    debug(query_res)

def test_case_16():
    DB_USER = "aliyun_ecs"
    DB_PASS = "sobey@2016"
    DB_HOST = "172.17.0.2"
    DB_PORT = "3306"
    DB_NAME = "aliyun_ecs_db"
    DATABASE_URI = 'mysql+mysqldb://%s:%s@%s:%s/%s' % (DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
    aliy_db = AliyunInstanceDBExecutor(db_url=DATABASE_URI)

    # query all
    debug("=====query all")
    query_res = aliy_db.query()
    debug(query_res)

    # query with filter
    debug("=====query with filter one")
    query_res = aliy_db.query(InstanceId="i-bp1e9zd4gslhgs03dlha")
    debug(query_res)

    debug("=====query with filter multiple")
    query_res = aliy_db.query(InstanceId="i-bp1e9zd4gslhgs03dlha",
                              ZoneId="cn-hangzhou-e",
                              InternetMaxBandwidthOut=1)

    debug(query_res)

def test_case_17():
    DB_USER = "aliyun_ecs"
    DB_PASS = "sobey@2016"
    DB_HOST = "172.17.0.2"
    DB_PORT = "3306"
    DB_NAME = "aliyun_ecs_db"
    DATABASE_URI = 'mysql+mysqldb://%s:%s@%s:%s/%s' % (DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
    aliy_db = AliyunInstanceDBExecutor(db_url=DATABASE_URI)

    # update table with filter one
    update_res = aliy_db.update(update_dict={"RegionId":"kkkkk"}, InstanceId="i-bp1e9zd4gslhgs03dlha")
    debug(update_res)

    # update table with filter multiple
    update_res = aliy_db.update(update_dict={"RegionId": "aaaaaa"}, InstanceId="i-bp1e9zd4gslhgs03dlha",
                                ZoneId="cn-hangzhou-e", InternetMaxBandwidthOut=1)
    debug(update_res)

    # update table all
    update_res = aliy_db.update(update_dict={"RegionId": "kkkkooo"})
    debug(update_res)

def test_case_18():

    DB_USER = "aliyun_ecs"
    DB_PASS = "sobey@2016"
    DB_HOST = "172.17.0.2"
    DB_PORT = "3306"
    DB_NAME = "aliyun_ecs_db"
    DATABASE_URI = 'mysql+mysqldb://%s:%s@%s:%s/%s' % (DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
    aliy_db = AliyunInstanceDBExecutor(db_url=DATABASE_URI)
    aliy_db.insert(InstanceId="i-bp19l585e4itj1j3i5zk", RegionId="cn-hangzhou",
                   ImageId="centos_7_03_64_40G_alibase_20170625.vhd", TaskCode="v-op-01",
                   InstanceType="ecs.n1.tiny", ZoneId="cn-hangzhou-e",
                   VSwitchId="vsw-bp1k3lwt03yps72i5n5a8",
                   SecurityGroupId="sg-bp1jb1dt5pdv12oenc6h",
                   InstanceName="test-ecs-01", Description="test ecs build.",
                   InternetChargeType="PayByTraffic", InstanceChargeType="PostPaid",
                   InternetMaxBandwidthIn=1, InternetMaxBandwidthOut=1,
                   HostName="fzb-test", Password="hxfzb12#$", SystemDiskCategory="cloud_efficiency",
                   SystemDiskSize=50, SystemDiskName="fzb-test-disk", SystemDiskDescription="for test.")
    # delete by filter one
    debug("=====delete by filter one")
    delete_res = aliy_db.delete(InstanceId="i-bp19l585e4itj1j3i5zk")
    debug(delete_res)

def test_case_19():

    DB_USER = "aliyun_ecs"
    DB_PASS = "sobey@2016"
    DB_HOST = "172.17.0.2"
    DB_PORT = "3306"
    DB_NAME = "aliyun_ecs_db"
    DATABASE_URI = 'mysql+mysqldb://%s:%s@%s:%s/%s' % (DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
    aliy_db = AliyunInstanceDBExecutor(db_url=DATABASE_URI)
    aliy_db.insert(InstanceId="i-bp19l585e4itj1j3i5zk", RegionId="cn-hangzhou",
                   ImageId="centos_7_03_64_40G_alibase_20170625.vhd", TaskCode="v-op-01",
                   InstanceType="ecs.n1.tiny", ZoneId="cn-hangzhou-e",
                   VSwitchId="vsw-bp1k3lwt03yps72i5n5a8",
                   SecurityGroupId="sg-bp1jb1dt5pdv12oenc6h",
                   InstanceName="test-ecs-01", Description="test ecs build.",
                   InternetChargeType="PayByTraffic", InstanceChargeType="PostPaid",
                   InternetMaxBandwidthIn=1, InternetMaxBandwidthOut=1,
                   HostName="fzb-test", Password="hxfzb12#$", SystemDiskCategory="cloud_efficiency",
                   SystemDiskSize=50, SystemDiskName="fzb-test-disk", SystemDiskDescription="for test.")
    # delete by filter one
    debug("=====delete by filter multiple")
    delete_res = aliy_db.delete(InstanceId="i-bp19l585e4itj1j3i5zk",
                                RegionId="cn-hangzhou", InternetMaxBandwidthOut=1)
    debug(delete_res)

def test_case_20():

    DB_USER = "aliyun_ecs"
    DB_PASS = "sobey@2016"
    DB_HOST = "172.17.0.2"
    DB_PORT = "3306"
    DB_NAME = "aliyun_ecs_db"
    DATABASE_URI = 'mysql+mysqldb://%s:%s@%s:%s/%s' % (DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
    aliy_db = AliyunInstanceDBExecutor(db_url=DATABASE_URI)
    aliy_db.insert(InstanceId="i-bp19l585e4itj1j3i5zk", RegionId="cn-hangzhou",
                   ImageId="centos_7_03_64_40G_alibase_20170625.vhd", TaskCode="v-op-01",
                   InstanceType="ecs.n1.tiny", ZoneId="cn-hangzhou-e",
                   VSwitchId="vsw-bp1k3lwt03yps72i5n5a8",
                   SecurityGroupId="sg-bp1jb1dt5pdv12oenc6h",
                   InstanceName="test-ecs-01", Description="test ecs build.",
                   InternetChargeType="PayByTraffic", InstanceChargeType="PostPaid",
                   InternetMaxBandwidthIn=1, InternetMaxBandwidthOut=1,
                   HostName="fzb-test", Password="hxfzb12#$", SystemDiskCategory="cloud_efficiency",
                   SystemDiskSize=50, SystemDiskName="fzb-test-disk", SystemDiskDescription="for test.")
    # delete all
    debug("=====delete all")
    delete_res = aliy_db.delete()
    debug(delete_res)

def test_case_21():
    # DescribeInstanceVncUrl
    d_res = aliy_op.DescribeInstanceVncUrl(InstanceId="i-bp107g4y1jk5epnj88uj")
    print(d_res)
    print("https://g.alicdn.com/aliyun/ecs-console-vnc/0.0.2/index.html?vncUrl=%s&instanceId=i-bp107g4y1jk5epnj88uj&isWindows=false" % (d_res["VncUrl"]))
    debug(d_res)

def test_case_22():
    m_res = aliy_op.ModifyInstanceVncPasswd(InstanceId="i-bp107g4y1jk5epnj88uj", VncPassword="888888")
    debug(m_res)

def test_case_23():
    # ModifyInstanceAttribute
    m_res = aliy_op.ModifyInstanceAttribute(InstanceId="i-bp1hjd0nam25316b2x0s", Password="hxfzb12#$%",
                                            HostName="fzb-i-bp1hjd0nam25316b2x0s", InstanceName="fzb_tttt",
                                            Description="test ecs modify.")
    debug(m_res)

def test_case_24():
    # must be UTC time.
    m_res = aliy_op.ModifyInstanceAutoReleaseTime(InstanceId="i-bp1hjd0nam25316b2x0s",
                                                  AutoReleaseTime="2017-07-18T20:30:00Z")
    debug(m_res)

def test_case_25():
    m_res = aliy_op.ModifyInstanceVpcAttribute(InstanceId="i-bp1hjd0nam25316b2x0s",
                                               VSwitchId="vsw-bp1k3lwt03yps72i5n5a8",
                                               PrivateIpAddress="172.16.5.24")
    debug(m_res)

def test_case_26():
    # DescribeInstanceAutoRenewAttribute
    debug("=====DescribeInstanceAutoRenewAttribute")
    d_res = aliy_op.DescribeInstanceAutoRenewAttribute(InstanceId="i-bp12aie5dccgcyagp0ww")
    debug(d_res)
    debug("=====DescribeUserData")
    d_res = aliy_op.DescribeUserData(InstanceId="i-bp12aie5dccgcyagp0ww")
    debug(d_res)

def test_case_30():
    s_res = aliy_op.StopInstance(InstanceId="i-bp1g6vdok72lr9edxq3m", ForceStop=True)
    debug(s_res)

def test_case_31():
    s_res = aliy_op.StartInstance(InstanceId="i-bp14t2yduyjgl0q0z32m")
    while True:
        st = aliy_op.DescribeInstanceStatus()
        debug(st)

if __name__ == "__main__":
    init(logpath=ALIYUN_ECS_OPERATION_LOG)
    # ak = "LTAIr599QLK8bBhU"
    # sk = "HD8PRGYt1WIOJhqIbLF89bQoO8g4uK"
    # region_id = "cn-hangzhou"
    region_id = "cn-shanghai"
    ak = "LTAIliklfIP300rx"
    sk = "uJRyp2uUmG0TWaXbuS0oKhb5BALgFo"
    # region_id = "cn-beijing"
    global aliy_op
    aliy_op = ALiYunInstanceOperation(ak, sk, region_id)
    # test_case_0()
    # test_case_1()
    # test_case_1()
    # test_case_2()
    test_case_3()
    # test_case_3_1()
    # test_case_4()
    # test_case_5()
    # test_case_6()
    # test_case_7()
    # test_case_8()
    # test_case_9()
    # test_case_10()
    # test_case_11()
    # test_case_12()
    # test_case_15()
    # test_case_16()
    # test_case_17()
    # test_case_18()
    # test_case_19()
    # test_case_20()
    # test_case_21()
    # test_case_22()
    # test_case_21()
    # test_case_23()
    # test_case_30()
    # test_case_24()
    # test_case_25()
    # test_case_26()
